var token = false;
var stepMinimized = false;
var leftPlacement = true;
var resume_step = parseInt(window.localStorage.getItem('resume_step'));
var crossDomain = true;
chrome.extension.onRequest.addListener(handleRequest);
chrome.extension.onMessage.addListener(function(msg, sender, sendResponse) {
    if (msg.action == 'checkAndRunWalkthrough') {
        chrome.storage.local.get("token", function(info) {
            if (info) {
                token = info.token;
            }
            console.log("page load sidebar \n" + token);
            checkAndRunWalkthrough();

            setTimeout(function() {
                window.dispatchEvent(new Event('resize'));
            }, 3000);
        });
    }
});

var TLDs = ["ac", "ad", "ae", "aero", "af", "ag", "ai", "al", "am", "an", "ao", "aq", "ar", "arpa", "as", "asia", "at", "au", "aw", "ax", "az", "ba", "bb", "bd", "be", "bf", "bg", "bh", "bi", "biz", "bj", "bm", "bn", "bo", "br", "bs", "bt", "bv", "bw", "by", "bz", "ca", "cat", "cc", "cd", "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "com", "coop", "cr", "cu", "cv", "cx", "cy", "cz", "de", "dj", "dk", "dm", "do", "dz", "ec", "edu", "ee", "eg", "er", "es", "et", "eu", "fi", "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf", "gg", "gh", "gi", "gl", "gm", "gn", "gov", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk", "hm", "hn", "hr", "ht", "hu", "id", "ie", "il", "im", "in", "info", "int", "io", "iq", "ir", "is", "it", "je", "jm", "jo", "jobs", "jp", "ke", "kg", "kh", "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la", "lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly", "ma", "mc", "md", "me", "mg", "mh", "mil", "mk", "ml", "mm", "mn", "mo", "mobi", "mp", "mq", "mr", "ms", "mt", "mu", "museum", "mv", "mw", "mx", "my", "mz", "na", "name", "nc", "ne", "net", "nf", "ng", "ni", "nl", "no", "np", "nr", "nu", "nz", "om", "org", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm", "pn", "pr", "pro", "ps", "pt", "pw", "py", "qa", "re", "ro", "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sr", "st", "su", "sv", "sy", "sz", "tc", "td", "tel", "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tp", "tr", "travel", "tt", "tv", "tw", "tz", "ua", "ug", "uk", "us", "uy", "uz", "va", "vc", "ve", "vg", "vi", "vn", "vu", "wf", "ws", "xn--0zwm56d", "xn--11b5bs3a9aj6g", "xn--3e0b707e", "xn--45brj9c", "xn--80akhbyknj4f", "xn--90a3ac", "xn--9t4b11yi5a", "xn--clchc0ea0b2g2a9gcd", "xn--deba0ad", "xn--fiqs8s", "xn--fiqz9s", "xn--fpcrj9c3d", "xn--fzc2c9e2c", "xn--g6w251d", "xn--gecrj9c", "xn--h2brj9c", "xn--hgbk6aj7f53bba", "xn--hlcj6aya9esc7a", "xn--j6w193g", "xn--jxalpdlp", "xn--kgbechtv", "xn--kprw13d", "xn--kpry57d", "xn--lgbbat1ad8j", "xn--mgbaam7a8h", "xn--mgbayh7gpa", "xn--mgbbh1a71e", "xn--mgbc0a9azcg", "xn--mgberp4a5d4ar", "xn--o3cw4h", "xn--ogbpf8fl", "xn--p1ai", "xn--pgbs0dh", "xn--s9brj9c", "xn--wgbh1c", "xn--wgbl6a", "xn--xkc2al3hye2a", "xn--xkc2dl3a5ee0h", "xn--yfro4i67o", "xn--ygbi2ammx", "xn--zckzah", "xxx", "ye", "yt", "za", "zm", "zw"].join();

var sidebarOpen = false;
var jsonObj = [];
var scheme = (window.location.protocol !== "http:" || window.location.hostname.indexOf('jobdiva') !== -1) ? "https://" : "http://";
var myDomOutline = DomOutline({
    onClick: domOutlineHandler,
    realtime: true,
    border: true,
    borderWidth: 4
});
var baseURL = scheme + "//api.wewalkthru.com";
//var baseURL = "http://localhost:3002"
function handleError() {
    jQuery('#sidebarframe').contents().find('body #userLogin').show();
    jQuery('#sidebarframe').contents().find('body #starter, body #listWlkthr, body #addWalkthrough, body .progress').remove();
    jQuery('#sidebarframe').contents().find('#viewWlkthrs').remove();
    jQuery('#sidebarframe').contents().find('body #walkthroughSideTable').parent('div.table-responsive').hide();
    jQuery('#sidebarframe').contents().find('body #walkthroughSideTable').detach();
    jQuery('#sidebarframe').contents().find('body #editorButtonsTbl').detach();
    jQuery('#sidebarframe').contents().find('body #editorRefPanel').detach();
    jQuery('#sidebarframe').contents().find('body #viewWlkthrs').show();
    jQuery('#sidebarframe').contents().find('body a.getWalkthroughData').show();
    jQuery('#sidebarframe').contents().find('body #toggleCode').hide();

}
jQuery.ajaxSetup({
    error: function(jqXHR, exception) {
        if (jqXHR.status === 403) {
            handleError();
        } else {
            alert('Uncaught Error.\n' + jqXHR.responseText);
        }
    }
});
jQuery.fn.extend({
    getPath: function() {
        var pathes = [];
        this.each(function(index, element) {
            var path, $node = jQuery(element);
            while ($node.length) {
                var realNode = $node.get(0),
                    name = realNode.localName;
                if (!name) {
                    break;
                }
                name = name.toLowerCase();
                var parent = $node.parent();
                var sameTagSiblings = parent.children(name);
                if (sameTagSiblings.length > 1) {
                    allSiblings = parent.children();
                    index = allSiblings.index(realNode) + 1;
                    if (index > 0) {
                        name += ':nth-child(' + index + ')';
                    }
                }
                path = name + (path ? ' > ' + path : '');
                $node = parent;
            }
            pathes.push(path);
        });
        return pathes.join(',');
    }
});
//Entry point of walkthrough here
function checkAndRunWalkthrough() {

    chrome.storage.local.get("CurrentWalkthrough", function(items) {
        if (!chrome.runtime.error && items != null && items != undefined && items.CurrentWalkthrough != undefined) {
            currentWalkthrough = items.CurrentWalkthrough;
            var hash = currentWalkthrough.Id;
            var storage = hash + '_current_step';
            resume_step = window.localStorage.setItem(storage, window.localStorage.getItem(storage));
            //getting walkthrough data across domain

            console.log(items);
            //
            jQuery.ajax({
                url: baseURL + "/api/walkthrough/getAllSteps?walkthroughId=" + hash,
                type: "GET",
                dataType: "json",
                headers: {
                    xaccesstoken: token
                },
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                success: function(data) {
                    var panel = jQuery('#sidebarframe').contents().find('#panelContent');
                    var pages = '';
                    var sortedElements = data.sort(function(o1, o2) {
                        return o1.PageOrderNo - o2.PageOrderNo
                    });
                    var check = [];
                    for (var i = 0; i < sortedElements.length; i++) {
                        check.push(getDomain(sortedElements[i].Subdomain));
                    }
                    check = unique(check);
                    chrome.storage.local.set({"BackUrl":check},function(){});

                    var found = jQuery.inArray(getDomain(window.location.host), check);
                    if (getDomainIdentifier(currentWalkthrough.WalkthroughForSite) == getDomainIdentifier(window.location.host) || found >= 0) {
                        setSelectedWalkthrough(currentWalkthrough.Id, false, items.CurrentWalkthrough.Description, items.CurrentWalkthrough.useAdditionalParams).done(function() {
                            runWalkthrough();
                        });
                    }

                }
            });



        }
    });
}
function getDomainIdentifier(URL){

  var siteURL = getDomain(URL).split(".");
  var Identifier="";
  if(siteURL[0]=='www'){
            Identifier = siteURL[1];
            return Identifier;
   }
  else{
    Identifier=siteURL[0];
    return Identifier;
  }

}
var domOutlineHandler = function(element) {
        console.log('Handle Ouline for:', element);
    }

function getPlainText(text){
    var txt = text;
    var rex = /(<([^>]+)>)/ig;
    return txt.replace(rex , " ").trim();;

}
    //give complete domain name as in window.location
function getDomainName(hostName) {
    return hostName.substring(hostName.lastIndexOf(".", hostName.lastIndexOf(".") - 1) + 1);
}

function getDomain(url) {
    var parts = url.split('.'),
        ln = parts.length,
        i = ln,
        minLength = parts[parts.length-1].length,
        part

    while(part = parts[--i]){
        if (TLDs.indexOf(part) < 0 || part.length < minLength || i < ln-2 || i === 0){
            return part;
        }
    }
}
// retturn unique elements of array
function unique(list) {
    var result = [];
    jQuery.each(list, function(i, e) {
        if (jQuery.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

var controllerControl = '<div style="text-align:center;width:100px;" class="controller"><span data-dir="top" class="top fa fa-2x fa-caret-up"></span><div class="clearfix"></div><span data-dir="left" class ="left fa fa-2x fa-caret-left"></span><span class="img-sq-25" style="display: inline-block;"></span><span class="right fa fa-2x fa-caret-right" data-dir="right"></span><div class="clearfix"></div><span data-dir="bottom" class ="bottom fa fa-2x fa-caret-down"></span></div>';

function handleRequest(request, sender, sendResponse) {
    if (request.callFunction == "toggleSidebar")
        toggleSidebar();
}

function toggleSidebar() {
    if (sidebarOpen && document.getElementById('sidebarframe') != null) {
        removeSideBar();
    } else {
        InitializeSideBar();
    }
}

function InitializeSideBar() {

    var focusStyle = '.glowing-border{outline: solid 1px red;border-color: red;box-shadow: 0 0 10px red;}';
    jQuery('<style type="text/css">' + focusStyle + '</style>').appendTo("head");
    var parentZoomOut = '.bodyzoomout{-webkit-transform: scale(0.6);-webkit-transform-origin: 0 0;transform: scale(0.6);transform-origin: 0 0;}';
    var sidebarzoomout = '.sidebarzoomout{width: calc(100% - 30px);margin:0 auto;}.spacer{margin:0; padding:0; height:30px;}.top25{top: 25%;position: relative;}.top15{top: 15%;position: relative;}.top20{top: 20%;position: relative;}';
    var panelCss = '.clickable{cursor: pointer;}.panel-heading span{margin-top: -20px;}.active-pos{color:#5cb85c!important;}.btn-active{color:#fff !important;background-color:#5cb85c!important;border-color: #4cae4c !important;}.top,.right,.bottom,.left{/*width:5px;height:20px;font-size: 5px;*/}.fr-command, .fr-btn {z-index:999 !important;}.fr-toolbar,.fr-inline{  z-index:9999999 !important;}'
    jQuery('<style type="text/css">' + parentZoomOut + '</style>').appendTo("head");
    var sidebar = jQuery('<iframe></iframe>')
        .attr('id', 'sidebarframe')
        .css({
            "width": "400px",
            "height": "100%",
            "display": "block",
            "position": "fixed",
            "z-index": "2147483647",
            "top": "0",
            "left": "0px",
            "padding": "20px",
            "box-sizing": "border-box",
            "background": "#FFF",
            "box-shadow": '1px 1px 5px rgba(0,0,0,.4)'
        });
    jQuery('body').append(sidebar);

    sidebar.ready(function() {
        var $head = jQuery("#sidebarframe").contents().find("head");
//        $head.append('<script type="text/javascript"src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"</script>');
        $head.append('<script type="text/javascript"src="' + chrome.extension.getURL("libs/jquery-2.1.1.min.js") + '"></script>');
        $head.append('<script type="text/javascript"src="' + chrome.extension.getURL("libs/bootstrap.min.js") + '"></script>');
        $head.append('<script type="text/javascript"src="' + chrome.extension.getURL("libs/material.min.js") + '"></script>');
        $head.append('<link type="text/css" rel="stylesheet" href="' + chrome.extension.getURL("libs/bootstrap.min.css") + '"/>');
        $head.append('<link type="text/css" rel="stylesheet" href="' + chrome.extension.getURL("libs/material.min.css") + '"/>');
        $head.append('<link type="text/css" rel="stylesheet" href="' + chrome.extension.getURL("libs/bootcomplete.css") + '"/>');
        $head.append('<link type="text/css" rel="stylesheet" href="' + chrome.extension.getURL("libs/custom.css") + '"/>');
        $head.append('<link type="text/css" rel="stylesheet" href="' + chrome.extension.getURL("libs/font-awesome.min.css") + '"/>');
        $head.append('<link type="text/css" rel="stylesheet" href="' + chrome.extension.getURL("libs/tinymce/skins/lightgray/skin.min.css") + '"/>');
        $head.append(jQuery('<style type="text/css">' + focusStyle + '</style>'));
        $head.append(jQuery('<style type="text/css">' + sidebarzoomout + '</style>'));
        $head.append(jQuery('<style type="text/css">' + panelCss + '</style>'));


        var toggleCodeButton = '<div><button id="toggleCode" style="display:none;">TextView</button><button id="Logout" style="float: right;display:none;">Logout</button> </div>';
        jQuery('#sidebarframe').contents().find('body').append(toggleCodeButton);

        var createLogin = "<div id='userLogin' class='col-sm-10 col-sm-offset-1 form-horizontal top25'><div class='form-group'><input type='text' class='form-control' placeholder='E-mail' id='login-email' required/></div><div class='form-group'><input type='password' class='form-control' placeholder='Password' id='login-password' required/></div><div class='form-group'><button type='button' id='loginUser' class='btn-wt btn-success-wt btn-block-wt'><i class='fa fa-user fa-fw'></i>Log In</button></div><div class='form-group'><input type='button'id='registrationtoggler' class='btn-wt btn-success-wt btn-block-wt' value='Register'/></div></div>";
        jQuery('#sidebarframe').contents().find('body').append(createLogin);

        var createRegister = "<div id='registrationForm' style='display:none;' class='col-sm-10 col-sm-offset-1 form-horizontal top25'><div class='form-group'><input type='text' class='form-control' placeholder='First Name' id='firstName'/></div><div class='form-group'>  <input type='text' class='form-control' placeholder='Last Name' id='lastName'/></div><div class='form-group'>  <input type='text' class='form-control' placeholder='Email' id='email'/></div><div class='form-group'>  <input type='password' class='form-control' placeholder='Password' id='password'/></div><div class='form-group'><input type='button'id='registerUser' class='btn-wt btn-success-wt btn-block-wt' value='Register'/></div><div class='form-group'><input type='button'id='logintoggler' class='btn-wt btn-success-wt btn-block-wt' value='Log In'/></div></div>";

        jQuery('#sidebarframe').contents().find('body').append(createRegister);

        var starter = '<div id="starter" class="control-group col-sm-8 col-sm-offset-2 top25" style="display:none;"><div class="row"><button type="button" id="createWlkthr" class="btn-wt btn-primary-wt btn-lg-wt btn-block-wt"><i class="fa fa-pencil fa-fw"></i>Create WalkThru</button></div><div class="spacer"></div><div class="row"><button type="button" id="listWlkthr" class="btn-wt btn-default-wt btn-lg-wt btn-block-wt">View Site WalkThru</button></div></div>';

        jQuery('#sidebarframe').contents().find('body').append(starter);

        var createWalkthrough = "<div id='addWalkthrough' style='display:none;' class='col-sm-10 col-sm-offset-1 form-horizontal top25'><input type='hidden' id='wlk_Id'/><div class='form-group'><input type='text' class='form-control' placeholder='Enter WalkThru Name' id='txt_walkthrough'/></div><div class='form-group'><textarea class='form-control' placeholder='Description' id='wlk_description'></textarea></div><div class='form-group'><input type='text' class='form-control' placeholder='Primary Category' id='wlk_category'/></div><div class='form-group'><input type='text' class='form-control' placeholder='Tags' id='wlk_tags'/></div><div class='form-group'><input type='text' style='display:none;'class='form-control' placeholder='Screenshot url' id='wlk_screen'/></div><div class='form-group'><input type='text' class='form-control' placeholder='icon' id='wlk_icon'/></div><div id='wlk_publish' style='display:none;'class='checkbox'> <label><input id='publish' type='checkbox' value=''>Publish</label></div><div id='wlk_additionalParams' style='display:none;'class='checkbox'> <label><input id='additionalParams' type='checkbox' value=''>Use Additional Params</label></div><div class='form-group'><input type='button'id='createWalkthrough' class='btn-wt btn-success-wt btn-block-wt' value='Create WalkThru'/><input type='button' id='updateWalkthrough' class='btn-wt btn-success-wt btn-block-wt' style='display:none' value='Update Walkthrough'/><input type='button' id='backFromCreate' class='btn-wt btn-success-wt btn-block-wt' value='Back'/></div></div>";
        jQuery('#sidebarframe').contents().find('body').append(createWalkthrough);
        sidebarOpen = true;
        jQuery("#sidebarframe").contents().find('body').addClass('sidebarzoomout wt-css');
        //jQuery('body').css('padding-left', '40%');
        jQuery('body').css('padding-left', '400px');

        RegisterButtonClicks();
        registerQueryStringPageElementsClick();
        addWalkthrough();
        getWalkthroughData();
        playWalkthrough();
        downloadWalkthroughLink();
        RegisterWindowUnload();

        var inter = window.setInterval(function() {
            if ($('#sidebarframe')[0].contentWindow.document.readyState === "complete") {
                loadLibs();
                window.clearInterval(inter);
                if (typeof jQuery !== undefined) {
                    $head.append('<script type="text/javascript"src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>');
                }
            }
        }, 100);

        login();
        register();
        chrome.storage.local.get("token", function(items) {
            if (!chrome.runtime.error) {
                token = items.token;
                var data={};
                data.site = window.location.host;
                jQuery.ajax({
                    url: baseURL + "/api/walkthrough/FindBySite?tagGroup=1&published=true&site=" + window.location.host,
                    type: "POST",
                    dataType: "json",
                    data:data,
                    headers: {
                        xaccesstoken: token
                    },
                    success: function(response) {
                        if(!response.success){
                            if(response.walkthrough.length>0 ){
                                    jQuery('#sidebarframe').contents().find('#userLogin').hide();
                                    jQuery('#sidebarframe').contents().find('#starter').show();
                                    jQuery('#sidebarframe').contents().find('#Logout').show();

                            }
                            //code to change as per access
                            console.log("No walkthroughs for you yet");
                        } 

                        else{   
                        jQuery('#sidebarframe').contents().find('#starter').show();
                        jQuery('#sidebarframe').contents().find('#Logout').show();
                        jQuery('#sidebarframe').contents().find('#userLogin').hide();
                        jQuery('#sidebarframe').contents().find('#registrationForm').hide();
                    }
                    },
                    error: function(err) {}
                });
            }
        });
    });
}

function loadLibs() {
    var frame = $('#sidebarframe')[0];
    var win = frame.contentWindow,
        doc = win.document,
        body = doc.body,
        jQueryLoaded = false,
        jQuery;

    function loadJQueryUI() {
        body.removeChild(jQuery);
        jQuery = null;

        window.jQuery.ajax({
            url: window.location.protocol + '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',
            dataType: 'script',
            cache: true,
            success: function() {}
        });
    }
    jQuery = doc.createElement('script');
    // based on https://gist.github.com/getify/603980
    jQuery.onload = jQuery.onreadystatechange = function() {
        if ((jQuery.readyState && jQuery.readyState !== 'complete' && jQuery.readyState !== 'loaded') || jQueryLoaded) {
            return false;
        }
        jQuery.onload = jQuery.onreadystatechange = null;
        jQueryLoaded = true;
        loadJQueryUI();
    };
    jQuery.src = window.location.protocol + '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js';
    body.appendChild(jQuery);
}

function loadJQueryUI() {
    return this.jQuery.ajax({
        url: window.location.protocol + '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',
        dataType: 'script',
        cache: true,
        success: function() {
            jQuery("#sidebarframe").contents().find('body').append('<script type="text/javascript"src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"</script>');
        },
        error: function(jqXHR, exception) {
            if (jqXHR.status === 403) {
                handleError();
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
}

function register() {
    jQuery('#sidebarframe').contents().find('#registerUser').on('click', function(event) {

        var data = {};
        data.fname = jQuery("#sidebarframe").contents().find("#firstName").val();
        data.lname = jQuery("#sidebarframe").contents().find("#lastName").val();
        data.password = jQuery("#sidebarframe").contents().find("#password").val();
        data.Username = data.email = jQuery("#sidebarframe").contents().find("#email").val();

        if (data.fname && data.lname && data.password && data.Username) {
            jQuery.ajax({
                url: baseURL + '/api/user/signup',
                type: "POST",
                data: data,
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                success: function(response) {
                    console.log(response);
                    $.alert({
                        title: 'Success!',
                        content: 'User created Successfully!',
                        confirm: function() {},
                        columnClass: 'confirm-width'
                    });
                    jQuery("#sidebarframe").contents().find('#registrationForm').hide();
                    jQuery("#sidebarframe").contents().find('#userLogin').show();
                },
                error: function(jqXHR, exception) {
                    if (jqXHR.status === 403) {
                        handleError();
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText);
                    }
                }

            });
        }

    });
}

function login() {
    jQuery('#sidebarframe').contents().find('#loginUser').on('click', function(event) {

        var data = {};

        data.password = jQuery("#sidebarframe").contents().find("#login-password").val();
        data.Username = data.email = jQuery("#sidebarframe").contents().find("#login-email").val();
        if (data.password && data.Username) {
            jQuery.ajax({
                url: baseURL + '/api/user/signin',
                type: "POST",
                data: data,

                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                success: function(response) {

                    console.log(response);
                    if (response.success === true) {
                        token = response.token;
                        chrome.storage.local.set({
                            "token": response.token
                        }, function() {
                            if (chrome.runtime.error) {
                                console.log("Runtime error.");
                            }
                            jQuery('#sidebarframe').contents().find('#userLogin').hide();
                            jQuery('#sidebarframe').contents().find('#starter').show();
                            jQuery('#sidebarframe').contents().find('body #Logout').show();
                            jQuery('#sidebarframe').contents().find('body #listWlkthr').show();
                        });
                    } else {
                        $.alert({
                            title: 'Error!',
                            content: response.message,
                            confirm: function() {},
                            columnClass: 'confirm-width'
                        });
                    }
                },
                error: function(jqXHR, exception) {
                    if (jqXHR.status === 403) {
                        handleError();
                    } else {
                        console.log('Uncaught Error.\n' + jqXHR.responseText);
                    }
                }
            });
        }

    });
}

function RegisterButtonClicks() {

     jQuery('#sidebarframe').contents().find('body').on('click', '#toggleCode', function() {

        toggleFrontCodeHtml();

         jQuery('#sidebarframe').contents().find('.rteLauncher').toggle();
        jQuery('#sidebarframe').contents().find('body #updatePageElems').toggle();


        jQuery(this).text(function(i, text){

            if(text=="TextView"){
                jQuery('#sidebarframe').contents().find('.wlkTxtAr').attr("readonly","true");
                return "CodeView"
            }
            else{
                  jQuery('#sidebarframe').contents().find('.wlkTxtAr').removeAttr("readonly");
                return "TextView"
            }

        })

     });
     jQuery('#sidebarframe').contents().find('body').on('click', '#Logout', function() {

        jQuery.ajax({
            url: baseURL + "/api/user/signout",
            type: "POST",
            dataType: "json",
            
            headers: {
                xaccesstoken: token
            },
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(resp) {
                 chrome.storage.local.set({"token":null, function(){}});
                  jQuery('#sidebarframe').contents().find('body #Logout').hide();
                jQuery('#sidebarframe').contents().find('body #userLogin').show();
                jQuery('#sidebarframe').contents().find('body #starter').hide();
                jQuery('#sidebarframe').contents().find('body #registrationForm').hide();
                jQuery('#sidebarframe').contents().find('body #addWalkthrough').hide();
                 jQuery('#sidebarframe').contents().find('body #viewWlkthrs').remove();
                  jQuery('#sidebarframe').contents().find('body #mySidebar').remove();
                jQuery('#sidebarframe').contents().find('body .progress').remove();
                jQuery('#sidebarframe').contents().find('body #toggleCode').hide();
                
                
            },
          });  
        
       
    }); 
    jQuery('#sidebarframe').contents().find('body').on('click', '#logintoggler', function() {

        jQuery('#sidebarframe').contents().find('body #userLogin').show();
        jQuery('#sidebarframe').contents().find('body #Logout').hide();
        jQuery('#sidebarframe').contents().find('body #registrationForm').hide();
    });
    jQuery('#sidebarframe').contents().find('body').on('click', '#registrationtoggler', function() {
        jQuery('#sidebarframe').contents().find('body #userLogin').hide();
        jQuery('#sidebarframe').contents().find('body #registrationForm').show();
    });
    jQuery('#sidebarframe').contents().find('body').on('click', '#createWlkthr', function() {
        jQuery('#sidebarframe').contents().find('body #userLogin, body #registrationForm, body #starter').hide();
        jQuery('#sidebarframe').contents().find('body #addWalkthrough').show();
    });
    jQuery('#sidebarframe').contents().find('body').on('click', '#backFromCreate', function() {
       
            jQuery('#sidebarframe').contents().find('body #starter').show();
        jQuery('#sidebarframe').contents().find('body #addWalkthrough').hide();
    });
    jQuery('#sidebarframe').contents().find('body').on('click', '#listWlkthr', function() {
        jQuery('#sidebarframe').contents().find('body #starter').hide();
        imgSrc = chrome.extension.getURL("libs/squares.gif");
            //<div class="progress-bar-wt progress-bar-striped-wt active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"><span class="sr-only">Working</span></div>
        var loader = '<div class="progress" style="margin-top:40%;margin-left:31%"><img src="'+imgSrc+'"/></div>';
        jQuery('#sidebarframe').contents().find('body').append(loader);

        var URL = getDomainIdentifier(window.location.host).split(".");
        var data = {};
        data.site = window.location.host;
        jQuery.ajax({
            url: baseURL + "/api/walkthrough/FindBySite?site=" + URL,
            type: "POST",
            dataType: "json",
            data:data,
            headers: {
                xaccesstoken: token
            },
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(resp) {

                if(resp.userInfo=="USER"){
                         $.alert({
                        title: 'Error!',
                        content: 'Your are not authorized.Please Contact your Admin',
                        confirm: function() {},
                        columnClass: 'confirm-width'
                    });
                           chrome.storage.local.set({"token":null, function(){}});
                           jQuery("#sidebarframe").contents().find('#userLogin').show();
                    jQuery("#sidebarframe").contents().find('.progress').remove();

                         return 0;
                }

                if(!resp.success && resp.success!=null){
                    jQuery("#sidebarframe").contents().find('#userLogin').show();
                    jQuery("#sidebarframe").contents().find('#starter').hide();
                    jQuery("#sidebarframe").contents().find('.progress').remove();
                    console.log("No walkthrough for you");
                    return 0 ;
                }
                var myWalkthroughs = "";
                var data = resp.walkthrough;
                if (Array.isArray(data)) {
                    for (i = 0; i < data.length; i++) {
                        myWalkthroughs += '<li class="list-group-item"><div class="table-disp"><div class="checkbox table-cell wid-50"><span class="editWlk" data-wlkId="' + data[i].Id + '"><i class="fa fa-tv fa-2x"></i></span></div><div class="action-buttons table-cell"><p class="noMargin editWlk" data-wlkId="' + data[i].Id + '"><label class="font80 text-uppercase">' + data[i].Name + '</label></p><p class="noMargin"><a class="getWalkthroughData" data-wlkid="' + data[i].Id + '"><i class="fa font140 fa-pencil-square-o fa-fw"></i></a><a class="playWalkthrough" data-wlkid="' + data[i].Id + '"><i class="fa font140 fa-play-circle-o fa-fw"></i></a><a class="downloadWalkthrough" data-wlkid="' + data[i].Id + '"><i class="fa font140 fa-download fa-fw"></i></a><a class="deleteWalkthrough" id="deleteWalkthrough" data-wlkid="' + data[i].Id + '"><i class="fa font140 fa-trash-o fa-fw"></i></a></p></div></div></li>';
                    }
                } else {
                    myWalkthroughs += '<li class="list-group-item"><div class="table-disp"><div class="checkbox table-cell wid-50"><span class="editWlk" data-wlkId="' + data[i].Id + '"><i class="fa fa-tv fa-2x"></i></span></div><div class="action-buttons table-cell"><p class="noMargin editWlk" data-wlkId="' + data.Id + '"><label class="font80 text-uppercase">' + data.Name + '</label></p><p class="noMargin"><a class="getWalkthroughData" data-wlkid="' + data[i].Id + '"><i class="fa font140 fa-pencil-square-o fa-fw"></i></a><a class="playWalkthrough" data-wlkid="' + data[i].Id + '"><i class="fa font140 fa-play-circle-o fa-fw"></i></a><a class="downloadWalkthrough" data-wlkid="' + data[i].Id + '"><i class="fa font140 fa-download fa-fw"></i></a></p></div></div></li>';

                }
                var walkthroughHtml = "<div id='viewWlkthrs' class='col-sm-12 top15'><div class='panel panel-default'><div class='panel-heading'> <h2 class='panel-title'>Site Walkthroughs</h2></div><div class='panel-body'><input type='text' style='padding-bottom:10px;' class='form-control' id='remoteWlkthrlst' name='remoteWlkthrlst' placeholder='Remote Walkthrough'/><a id='editRemoteWlkThru'><i class='fa font160 fa-pencil-square-o fa-fw'></i></a><ul class='list-group'>" + myWalkthroughs + "</ul></div></div><div class='col-xs-12'><div class='row'><button type='button' id='backFromList' class='btn-wt btn-primary-wt btn-lg-wt btn-block-wt'>Back</button></div></div></div>"
                jQuery('#sidebarframe').contents().find('body #viewWlkthrs').remove();
                jQuery('#sidebarframe').contents().find('body').append(walkthroughHtml);
                jQuery('#sidebarframe').contents().find('body .progress').remove();
                getRemoteWalkthrough();
                editRemoteWlkThru();
            },
            error: function(jqXHR, textStatus, errorThrown) {

                if (jqXHR.status === 403) {
                    handleError();
                } else {

                    $.alert({
                        title: 'Error!',
                        content: 'Error in fetching Walkthroughs!',
                        confirm: function() {},
                        columnClass: 'confirm-width'
                    });
                    jQuery('#sidebarframe').contents().find('body .progress').remove();
                }
            }
        });
    });
    jQuery('#sidebarframe').contents().find('body').on('click', '#backFromList', function() {
        jQuery('#sidebarframe').contents().find('body #starter').show();
        jQuery('#sidebarframe').contents().find('body #viewWlkthrs').hide();
    });
    jQuery('#sidebarframe').contents().find('body').on('click', '.editWlk', function(event) {
        event.stopImmediatePropagation();
        var Id = jQuery(this).data('wlkid');
        showEditView(Id);
    });
    jQuery('#sidebarframe').contents().find('body').on('click', '#updateWalkthrough', function() {
        updateWalkthrougData();
    });
    jQuery('#sidebarframe').contents().find('body').on('click', '#deleteWalkthrough', function(event) {
        event.stopImmediatePropagation();
        var data = $(this).attr('data-wlkid');


        deleteWalkthrough(data, $(this).parent().parent().parent().parent().parent());

    });
}

function deleteWalkthrough(id,selector) {
    data = {};
    data.walkthroughId = id;
    jQuery.ajax({
        url: baseURL + "/api/walkthrough/deleteWalkthrough",
        data: data,
        type: "POST",
        dataType: "json",
        headers: {
            xaccesstoken: token
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function(output) {
            if (output) {

                selector.remove();

            } else
                alert("some error has occured");

        },
        error: function(jqXHR, textStatus, errorThrown) {},
    });



}

function showEditView(id) {
     imgSrc = chrome.extension.getURL("libs/squares.gif");
            //<div class="progress-bar-wt progress-bar-striped-wt active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"><span class="sr-only">Working</span></div>
        var loader = '<div class="progress" style="margin-top:40%;margin-left:31%"><img src="'+imgSrc+'"/></div>';
        jQuery('#sidebarframe').contents().find('body').append(loader);
    
    jQuery('#sidebarframe').contents().find('body #viewWlkthrs').hide();
    jQuery('#sidebarframe').contents().find('body #addWalkthrough').show();
    jQuery('#sidebarframe').contents().find('body #createWalkthrough').hide();
    jQuery('#sidebarframe').contents().find('body #wlk_screen').show();
    jQuery('#sidebarframe').contents().find('body #updateWalkthrough').show();
    jQuery('#sidebarframe').contents().find('body #wlk_publish').show();
    jQuery('#sidebarframe').contents().find('body #wlk_additionalParams').show();

    jQuery.ajax({
        url: baseURL + '/api/walkthrough/findById?walkthroughId=' + id,
        type: "GET",
        dataType: "json",
        headers: {
            xaccesstoken: token
        },
        success: function(data) {
            jQuery('#sidebarframe').contents().find('body .progress').remove();
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #wlk_Id').val(data.Id);
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #txt_walkthrough').val(data.Name);
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #wlk_description').val(data.Description);
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #wlk_category').val(data.PrimaryCategory);
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #wlk_tags').val(data.Tags);
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #wlk_screen').val(data.ScreenshotImage);
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #publish').prop('checked', data.IsPublished);
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #additionalParams').prop('checked', data.useAdditionalParams);
            jQuery('#sidebarframe').contents().find('body #addWalkthrough #wlk_icon').val(data.Icon);

        },
        error: function(jqXHR, textStatus, errorThrown) {

            if (jqXHR.status === 403) {
                handleError();
            } else {



                $.alert({
                    title: 'Error!',
                    content: 'Error in fetching Walkthrough data!',
                    confirm: function() {},
                    columnClass: 'confirm-width'
                });
                jQuery('#sidebarframe').contents().find('body .progress').remove();
            }
        }
    });
}

function getRemoteWalkthrough() {
    var url = baseURL + "/api/walkthrough/FindBySite";
    jQuery('#sidebarframe').contents().find('#remoteWlkthrlst').bootcomplete({
        url: url,
        method: 'get',
        minLength: 6
    });
}

function editRemoteWlkThru() {
    jQuery('#sidebarframe').contents().find('#editRemoteWlkThru').on('click', function(event) {
        event.stopImmediatePropagation();
        var Id = jQuery('#sidebarframe').contents().find('#remoteWlkthrlst_id').val();
        var name = jQuery('#sidebarframe').contents().find('#remoteWlkthrlst').val();
        var createMode = true;
        setSelectedWalkthrough(Id, createMode, name, false)
    });
}
//left unedited from previous version no ajax in this #Unchanged1
function getWalkthroughData() {
    jQuery('#sidebarframe').contents().find('body').on('click', '.getWalkthroughData', function(event) {
        event.stopImmediatePropagation();
        $(this).hide();
        jQuery('#sidebarframe').contents().find('body #toggleCode').show();

        var Id = jQuery(this).data("wlkid");
        var name = jQuery(this).text();
        var createMode = true;
        setSelectedWalkthrough(Id, createMode, name, false);

    });
}

function playWalkthrough() {
    jQuery('#sidebarframe').contents().find('body').on('click', '.playWalkthrough', function(event) {

        event.stopImmediatePropagation();
        var Id = jQuery("#sidebarframe").contents().find(this).data("wlkid");
        jQuery.ajax({
            url: baseURL + "/api/walkthrough/getFirstStep?walkthroughId=" + Id,
            type: "GET",
            headers: {
                xaccesstoken: token
            },
            success: function(data) {
                var name = jQuery("#sidebarframe").contents().find(this).text()
                var createMode = false;
                setSelectedWalkthrough(Id, createMode, name, data.useAdditionalParams).then(function() {
                    window.location.href = data.Page;
                });

            },
            error: function(jqXHR, textStatus, errorThrown) {

                if (jqXHR.status === 403) {
                    handleError();
                } else {


                    $.alert({
                        title: 'Error!',
                        content: 'Error in fetching Walkthrough data!',
                        confirm: function() {},
                        columnClass: 'confirm-width'
                    });
                }
            }
        });
    });
}

function downloadWalkthroughLink() {
    jQuery('#sidebarframe').contents().find('body').on('click', '.downloadWalkthrough', function() {
        var Id = jQuery("#sidebarframe").contents().find(this).data("wlkid");
        jQuery.ajax({
            url: baseURL + "/api/walkthrough/download?id=" + Id,
            type: "GET",
            headers: {
                xaccesstoken: token
            },
            success: function(data) {
                $.dialog({
                    title: 'Copy and embed the link!',
                    content: '<input type="text" style="width:80%" value="' + data.Location + '" id="copyme"/> <button id="clickme">Copy</button>',
                    columnClass: 'confirm-width'
                });
                copyHelper();
            },
            error: function(jqXHR, textStatus, errorThrown) {

                if (jqXHR.status === 403) {
                    handleError();
                } else {
                    $.alert({
                        title: 'Error!',
                        content: 'Error in fetching Walkthrough link!',
                        confirm: function() {},
                        columnClass: 'confirm-width'
                    });
                }
            }
        });
    });
}

function setSelectedWalkthrough(walkthroughId, mode, name, useQS) {

    var item = {};
    item["walkthroughId"] = walkthroughId;
    item["site"] = window.location.host;
    // item["page"] = useQS ? window.location.pathname +  window.location.search : window.location.pathname;

    item["page"] = useQS ? window.location.pathname : window.location.pathname;
    if (mode) {
        jQuery('#sidebarframe').contents().find('body #viewWlkthrs').hide();
         imgSrc = chrome.extension.getURL("libs/squares.gif");
            //<div class="progress-bar-wt progress-bar-striped-wt active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"><span class="sr-only">Working</span></div>
        var loader = '<div class="progress" style="margin-top:40%;margin-left:31%"><img src="'+imgSrc+'"/></div>';
        jQuery('#sidebarframe').contents().find('body').append(loader);
    }
    return jQuery.ajax({
        url: baseURL + "/api/walkthroughElements/getStepByPage",
        data: item,
        type: "GET",
        dataType: "json",
        headers: {
            xaccesstoken: token
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function(data) {
            if (data != null && data != undefined && data.WalkthroughElements.length > 0) {
                var currentWalkthrough = data.Walkthrough;
                chrome.storage.local.set({
                    "CurrentWalkthrough": currentWalkthrough
                }, function() {
                    if (chrome.runtime.error) {
                        console.log("Runtime error.");
                    }

                    var currentWalkthroughDetails = {};
                    currentWalkthroughDetails['elements'] = data.WalkthroughElements;
                    currentWalkthroughDetails['forPage'] = data.Page;
                    currentWalkthroughDetails['AdditionalParam'] = data.AdditionalParam;
                    currentWalkthroughDetails['walkthroughId'] = data.WalkthroughId;
                    currentWalkthroughDetails['walkthroughStepId'] = data.StepId;
                    currentWalkthroughDetails['backdrop'] = data.HasBackDrop != null || data.HasBackDrop != undefined ? data.HasBackDrop : false;
                    chrome.storage.local.set({
                        "CurrentWalkthroughDetails": currentWalkthroughDetails
                    }, function() {
                        if (chrome.runtime.error) {
                            console.log("Runtime error.");
                        }

                        var output = data.WalkthroughElements;

                        walkthroughObj = [];
                        for (var i = 0; i < output.length; i++) {
                            var item = {}
                            item["element"] = output[i].ElementHtmlId;
                            item["title"] = output[i].Title;
                            item["heading"] = output[i].Heading;
                            item["content"] = output[i].Description;
                            item["sno"] = output[i].SerialNumber;
                            item["type"] = output[i].ElementTypeHtml;
                            item["elementId"] = output[i].ElementId;
                            item["placement"] = output[i].Placement;
                            item["backdrop"] = output[i].HasBackDrop;
                            item["trigger"] = output[i].trigger;
                            item["duration"] = output[i].Duration;
                            item["delay"] = output[i].Delay;
                            walkthroughObj.push(item);
                        };
                        if (mode) {
                            jQuery('#sidebarframe').contents().find('body .progress').remove();
                            showWalkthrough();
                            jQuery('#sidebarframe').contents().find('body #savePageElems').hide();
                            jQuery('#sidebarframe').contents().find('body #updatePageElems').show();
                            AddManualElementsToForm(walkthroughObj);
                            fillFormValues(walkthroughObj, currentWalkthroughDetails['backdrop']);
                            getAllReferencePages(data.WalkthroughId);
                            setElementPlacement();
                        }
                    });
                });
            } else {
                if (mode) {
                    jQuery('#sidebarframe').contents().find('body .progress').remove();
                    var currentWalkthrough = {};
                    currentWalkthrough['Id'] = walkthroughId;
                    currentWalkthrough['Name'] = name;
                    currentWalkthrough['isSinglePage'] = false; // fetch from db later
                    currentWalkthrough['WalkthroughForSite'] = window.location.host;
                    currentWalkthrough['UserId'] = null;
                    chrome.storage.local.set({
                        "CurrentWalkthrough": currentWalkthrough
                    }, function() {
                        if (chrome.runtime.error) {
                            console.log("Runtime error.");
                        }
                        showWalkthrough();
                        getAllReferencePages(walkthroughId);
                        setElementPlacement();
                    });
                }
            }
            //   if(mode)
            //   {
            //   setUpEditor();

            // }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 403) {
                handleError();
            } else {
                $.alert({
                    title: 'Error!',
                    content: 'Error in getting Walkthrough element!',
                    confirm: function() {},
                    columnClass: 'confirm-width'
                });
            }
        }
    });
}

function removeSideBar() {
    jQuery('body').css('padding-left', '0%');
    jQuery('body').css('padding-right', '0%');
    jQuery('#sidebarframe').remove();
    myDomOutline.stop();
    sidebarOpen = false;


}

function addWalkthrough() {
    jQuery('#sidebarframe').contents().find('body').on('click', '#createWalkthrough', function() {
        var name = jQuery("#sidebarframe").contents().find("#txt_walkthrough").val();
        var item = {};
        item["name"] = name;
        item["description"] = jQuery("#sidebarframe").contents().find("#wlk_description").val();
        item["category"] = jQuery("#sidebarframe").contents().find("#wlk_category").val();
        item["tags"] = jQuery("#sidebarframe").contents().find("#wlk_tags").val();
        item["site"] = window.location.host;
        item["icon"] = jQuery("sidebarframe").contents().find('#wlk_icon').val();

        jQuery.ajax({
            url: baseURL + "/api/walkthrough/create",
            data: item,
            type: "POST",
            dataType: "json",
            headers: {
                xaccesstoken: token
            },
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {
                chrome.storage.local.set({
                    "CurrentWalkthrough": data
                }, function() {
                    if (chrome.runtime.error) {
                        console.log("Runtime error.");
                    }
                    showWalkthrough();
                    setElementPlacement();
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 403) {
                    handleError();
                } else {
                    $.alert({
                        title: 'Error!',
                        content: 'Error in creating Walkthrough!',
                        confirm: function() {},
                        columnClass: 'confirm-width'
                    });
                }
            }
        });
    });
}

function updateWalkthrougData() {

    var item = {};
    item["name"] = jQuery("#sidebarframe").contents().find("#txt_walkthrough").val();
    item["description"] = jQuery("#sidebarframe").contents().find("#wlk_description").val();
    item["category"] = jQuery("#sidebarframe").contents().find("#wlk_category").val();
    item["tags"] = jQuery("#sidebarframe").contents().find("#wlk_tags").val();
    item["screenshot"] = jQuery("#sidebarframe").contents().find("#wlk_screen").val();
    item["id"] = jQuery("#sidebarframe").contents().find("#wlk_Id").val();
    item["site"] = window.location.host;
    item["icon"] = jQuery("#sidebarframe").contents().find("#wlk_icon").val();
    item["isPublished"] = jQuery("#sidebarframe").contents().find("#publish").is(':checked');
    item['additionalParams'] = jQuery("#sidebarframe").contents().find("#additionalParams").is(':checked');
    jQuery.ajax({
        url: baseURL + "/api/walkthrough/update",
        data: item,
        type: "POST",
        dataType: "json",
        headers: {
            xaccesstoken: token
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function(data) {
            $.alert({
                title: 'Success!',
                content: 'Walkthrough data updated!',
                confirm: function() {}
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 403) {
                handleError();
            } else {
                $.alert({
                    title: 'Error!',
                    content: 'Error in Creating Walkthrough!',
                    confirm: function() {}
                });
            }
        }
    });
}
function toggleFrontCodeHtml(){
    var allelement = jQuery('#sidebarframe').contents().find('#mySidebar .elements');
     jQuery(allelement).each(function() {
        if (jQuery(this).find('.selectme').is(':checked')) {
            var a = jQuery(this).find('td textarea.wlkTxtAr').val() == '' ? '' : jQuery(this).find('td textarea.wlkTxtAr').val();
            var b = jQuery(this).find('td input.wlkTxtArCopy').val() == '' ? '' : jQuery(this).find('td input.wlkTxtArCopy').val();

            jQuery(this).find('td textarea.wlkTxtAr').val(b)
            jQuery(this).find('td input.wlkTxtArCopy').val(a);

        }

    });

}

function getSelectedElements() {
    var allelement = jQuery('#sidebarframe').contents().find('#mySidebar .elements');
    jsonObj = [];
    jQuery(allelement).each(function() {
        if (jQuery(this).find('.selectme').is(':checked')) {
            var item = {};
            var element = jQuery(this).find('td textarea.wlkTxtAr').attr('data-selector');
            var title = jQuery(this).find('td:nth-child(3) input').val() == '' ? jQuery(this).find('td:nth-child(2)').text() : jQuery(this).find('td:nth-child(3) input').val();
            var content = jQuery(this).find('td textarea.wlkTxtAr').val() == '' ? 'Walkthrough' : jQuery(this).find('td textarea.wlkTxtAr').val();
            var Heading =jQuery(this).find('td input.heading').val();

            item["element"] = element;
            item["title"] = title;
            item["heading"] = Heading;
            item["content"] = content;
            item["sno"] = parseInt(jQuery(this).find('td:nth-child(1)').text());
            item["type"] = jQuery(this).find('td:nth-child(2)').text();
            var orderNumber = title.split('Step ')[1];
            item["orderNumber"] = parseInt(orderNumber);
            item["page"] = window.location.host + window.location.pathname;
            var elementId = jQuery(this).find('td .elementId').val();
            item["elementId"] = elementId;
            item["placement"] = jQuery(this).find('td input.placement').val();
            item["backdrop"] = jQuery(this).find('td input.hasbackdrop').is(':checked');
            item["trigger"] = jQuery(this).find('td input.trigger').is(':checked');
            item["duration"] = jQuery(this).find('td input.duration').val();
            item["delay"] = jQuery(this).find('td input.delay').val();
            console.log(item);
            jsonObj.push(item);
        }
    });
}


function getAllReferencePages(walkthroughId) {
    jQuery.ajax({
        url: baseURL + "/api/walkthrough/getAllSteps?walkthroughId=" + walkthroughId,
        type: "GET",
        dataType: "json",
        headers: {
            xaccesstoken: token
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function(data) {
            var panel = jQuery('#sidebarframe').contents().find('#panelContent');
            var pages = '';
            var sortedElements = data.sort(function(o1, o2) {
                return o1.PageOrderNo - o2.PageOrderNo
            });
            for (var i = 0; i < sortedElements.length; i++) {
                var order;
                if (sortedElements[i].PageOrderNo) {
                    order = sortedElements[i].PageOrderNo;
                } else {
                    order = i + 1;
                }
                pages += '<tr><td class="srno" >' + order + '</td><td><div class="table-disp"><div class="checkbox table-cell wid-50"><span class="fa fa-tv fa-2x"></span></div><div class="table-cell"><p class="noMargin"><b>Domain:</b>' + sortedElements[i].Subdomain + '</p><p  id="walkthroughPageId" data-pagestepid="' + data[i].StepId + '"><b>Page:</b>' + sortedElements[i].Page + '</p></div></div><i class="fa fa-trash-o" aria-hidden="true" id="deleteWalkthruPage" style="float: right;margin: -9px;cursor: pointer;"data-stepId="' + sortedElements[i].StepId + '"> delete</i></td><td><input type="text" id="alias" placeholder="Enter comma seperated values for page link" value="' + sortedElements[i].Alias + '" ></td></tr>';
            }
            var list = "<table class='table' id='pagesTable'><tbody>" + pages + "</tbody></table>";
            panel.append(list);

        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 403) {
                handleError();
            } else {

                console.log("Reference pages not found or an error occured; " + textStatus);
            }
        }
    });
}

function registerQueryStringPageElementsClick() {
    jQuery('#sidebarframe').contents().find('body').on('click', '#walkthroughPageId', function(event) {
        event.stopImmediatePropagation();
        var selector = $(this);
        console.log(selector);

        var data = {
            stepId: $(this).attr('data-pagestepid')
        };
        console.log(data);
        jQuery.ajax({
            url: baseURL + "/api/walkthroughElements/getElementsByStep?stepId=" + data.stepId,
            data: data,
            type: "GET",
            dataType: "json",
            headers: {
                xaccesstoken: token
            },
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(output) {

                console.log(output);
                var walkthroughObj = [];
                for (var i = 0; i < output.length; i++) {
                    var item = {}
                    item["element"] = output[i].ElementHtmlId;
                    item["title"] = output[i].Title;
                    item["content"] = output[i].Description;
                    item["sno"] = output[i].SerialNumber;
                    item["type"] = output[i].ElementTypeHtml;
                    item["elementId"] = output[i].ElementId;
                    item["placement"] = output[i].Placement;
                    item["backdrop"] = output[i].HasBackDrop;
                    item["duration"] = output[i].Duration;
                    item["delay"] = output[i].Delay;
                    walkthroughObj.push(item);
                };

                fillFormValues(walkthroughObj, false);


            },
            error: function(jqXHR, textStatus, errorThrown) {},
        });

    });
}

function registerDeletePageClick() {
    jQuery('#sidebarframe').contents().find('body').on('click', '#deleteWalkthruPage', function(event) {
        event.stopImmediatePropagation();

        var selector = $(this);
        var data = {
            stepId: $(this).data('stepid')
        };

        jQuery.ajax({
            url: baseURL + "/api/walkthrough/delete",
            data: data,
            type: "POST",
            dataType: "json",
            headers: {
                xaccesstoken: token
            },
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function(data) {
                selector.parent().parent().remove();
                alert("deleted");


            },
            error: function(jqXHR, textStatus, errorThrown) {},
        });
    });
}

function registerSaveElemClick() {
    jQuery('#sidebarframe').contents().find('body').on('click', '#savePageElems', function(event) {
        event.stopImmediatePropagation();
        jQuery('#sidebarframe').contents().find('#savePageElems').prop('disabled', true);
        getSelectedElements();
        var currentWalkthrough;
        var backdrop = $('#sidebarframe').contents().find('#walkthroughSideTable').next().find('tr td.pageBackDrop input').is(':checked');
        chrome.storage.local.get("CurrentWalkthrough", function(items) {
            if (!chrome.runtime.error) {
                currentWalkthrough = items.CurrentWalkthrough;
                jQuery.ajax({
                    url: baseURL + "/api/walkthroughElements/createElement",
                    data: {
                        data: JSON.stringify(jsonObj),
                        page: window.location.pathname,
                        subdomain: window.location.origin,
                        additionPara: window.location.search + window.location.hash,
                        walkthroughId: currentWalkthrough.Id,
                        backdrop: backdrop
                    },
                    type: "POST",
                    dataType: "json",
                    headers: {
                        xaccesstoken: token
                    },
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    success: function(data) {
                        $.alert({
                            title: 'Success!',
                            content: 'Walkthrough Elements Added Successfully!',
                            confirm: function() {},
                            columnClass: 'confirm-width'
                        });

                        chrome.storage.local.set({
                            "CurrentWalkthroughDetails": data
                        }, function() {
                            if (chrome.runtime.error) {
                                console.log("Runtime error.");
                            }

                            jQuery('#sidebarframe').contents().find('#savePageElems').hide();
                            jQuery('#sidebarframe').contents().find('#updatePageElems').show();
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 403) {
                            handleError();
                        } else {
                            $.alert({
                                title: 'Error!',
                                content: 'Error in Adding Elements To Walkthrough!',
                                confirm: function() {},
                                columnClass: 'confirm-width'
                            });
                            jQuery('#sidebarframe').contents().find('#savePageElems').prop('disabled', false);
                        }
                    }
                });
            }
        });
    });
}

function registerUpdateClick() {
    jQuery('#sidebarframe').contents().find('body').on('click', '#updatePageElems', function(event) {
        event.stopImmediatePropagation();
        jQuery('#sidebarframe').contents().find('#updatePageElems').prop('disabled', true);
        getSelectedElements();
        var currentWalkthrough;
        var backdrop = $('#sidebarframe').contents().find('#walkthroughSideTable').next().find('tr td.pageBackDrop input').is(':checked');
        chrome.storage.local.get("CurrentWalkthroughDetails", function(items) {
            if (!chrome.runtime.error) {
                currentWalkthrough = items.CurrentWalkthroughDetails;
                jQuery.ajax({
                    url: baseURL + "/api/walkthroughElements/updateElement",
                    data: {
                        data: JSON.stringify(jsonObj),
                        page: window.location.pathname,
                        subdomain: window.location.origin,
                        additionPara: window.location.search + window.location.hash,
                        walkthroughId: currentWalkthrough.walkthroughId,
                        stepId: currentWalkthrough.walkthroughStepId,
                        backdrop: backdrop
                    },
                    type: "POST",
                    dataType: "json",
                    headers: {
                        xaccesstoken: token
                    },
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    success: function(data) {
                        $.alert({
                            title: 'Success!',
                            content: 'Walkthrough Elements Updated Successfully!',
                            confirm: function() {},
                            columnClass: 'confirm-width'
                        });
                        chrome.storage.local.set({
                            "CurrentWalkthroughDetails": data
                        }, function() {
                            if (chrome.runtime.error) {
                                console.log("Runtime error.");
                            }
                            jQuery('#sidebarframe').contents().find('#updatePageElems').prop('disabled', false);
                        });

                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                        if (jqXHR.status === 403) {
                            handleError();
                        } else {

                            $.alert({
                                title: 'Error!',
                                content: 'Error in Adding Elements To Walkthrough!',
                                confirm: function() {},
                                columnClass: 'confirm-width'
                            });
                            jQuery('#sidebarframe').contents().find('#updatePageElems').prop('disabled', false);
                        }
                    }
                });
            }
        });
    });
}

function registerBackFromEditScreenClick() {
    jQuery('#sidebarframe').contents().find('body').on('click', '#backFromEditScr', function() {
        jQuery('#sidebarframe').contents().find('body #walkthroughSideTable').parent('div.table-responsive').hide();
        jQuery('#sidebarframe').contents().find('body #walkthroughSideTable').detach();
        jQuery('#sidebarframe').contents().find('body #editorButtonsTbl').detach();
        jQuery('#sidebarframe').contents().find('body #editorRefPanel').detach();
        jQuery('#sidebarframe').contents().find('body #viewWlkthrs').show();
        jQuery('#sidebarframe').contents().find('body a.getWalkthroughData').show();
        jQuery('#sidebarframe').contents().find('body #toggleCode').hide();

    });
}

function registerRunClick() {
    jQuery('#sidebarframe').contents().find('body').on('click', '#runWalkthrough', function(e) {
        e.stopImmediatePropagation();
        window.location.reload();
        settimeout(function(){runWalkthrough()},1000);
    });
}

function runWalkthrough() {
    //window.localStorage.clear();
    chrome.storage.local.get("CurrentWalkthrough", function(currentWalk) {
        chrome.storage.local.get("CurrentWalkthroughDetails", function(data) {
            if (!chrome.runtime.error) {
                data = data.CurrentWalkthroughDetails;
                var requestByPage = {};
                requestByPage["page"] = window.location.host + window.location.pathname;
                requestByPage["stepId"] = data.walkthroughStepId;
                var path = data.forPage;
                var sortedElements = data.elements.sort(function(o1, o2) {
                    return o1.OrderNumber - o2.OrderNumber
                });
                walkthroughObj = [];
                for (var i = 0; i < sortedElements.length; i++) {
                    var item = {}
                    item["element"] = sortedElements[i].ElementHtmlId;
                    item["title"] = sortedElements[i].Title;
                    item["heading"] = sortedElements[i].Heading;
                    item["content"] = sortedElements[i].Description;
                    item["sno"] = sortedElements[i].SerialNumber;
                    item["type"] = sortedElements[i].ElementTypeHtml;
                    item["placement"] = sortedElements[i].Placement;
                    item["backdrop"] = sortedElements[i].HasBackDrop;
                    item["duration"] = sortedElements[i].Duration;
                    item["delay"] = (sortedElements[i].Delay) ? sortedElements[i].Delay : false;
                    item["ElementId"] = sortedElements[i].ElementId;
                    item["StepId"] = sortedElements[i].StepId;
                    item["trigger"] = sortedElements[i].trigger;
                    //add trigger item here 
                    walkthroughObj.push(item);
                    console.log(item);
                };

                setPopUpTemplate(walkthroughObj);
                //getting all pages

                startTour(walkthroughObj, data.walkthroughId, data.backdrop, currentWalk.CurrentWalkthrough.WalkthroughForSite);
                removeSideBar();
            }
        });
    });
}

function constructRow(row_no,elem,ctrl,style){
  if(style){
    var row = "<tr style='" + style + "' class='elements'>";
  }else {
    var row = '<tr class="warning elements">';
  }
        row += '<td class="srno">' + row_no + '</td>';
        row += '<td class="text-center" style="vertical-align: middle;width:20%;display:none;">' + elem.type + '</td>';
        row += '<td class="text-center" style="vertical-align: middle;width:10%"><input type="text" readonly class="img-sq-50 pad-top-10 pad-bot-10 text-center" style="border: 2px solid #FFC000;"></td>';

    if(style){
      row += '<td class="text-left" style="vertical-align: middle;" width="40%"><textarea class="wlkTxtAr" data-selector="' + elem.id + '" id="' + elem.id + '" rows="3"></textarea><button type="button" class="btn-wt btn-info-wt btn-xs-wt rteLauncher" data-toggle="modal-wt" style="display: -webkit-box"><i class="fa fa-pencil"></i></button><button type="button" class="btn-wt btn-info-wt btn-xs-wt hideeditor" >Close Editor</button></td>';
    }else {
      row += '<td class="text-left" style="vertical-align: middle;" width="40%"><textarea class="wlkTxtAr" data-selector="' + elem.element + '" id="' + elem.element + '" rows="3"></textarea><button type="button" class="btn-wt btn-info-wt btn-xs-wt rteLauncher" data-toggle="modal-wt" style="display: -webkit-box"><i class="fa fa-pencil"></i></button><button type="button" class="btn-wt btn-info-wt btn-xs-wt hideeditor" >Close Editor</button></td>';
    }
        row += '<td style="vertical-align: middle;text-align:center" width="20%"><input type="checkbox" class="selectme check" /></td>';
    row += '<td style="display:none;"><input type="hidden" class="elementId" /></td>';
        row += '<td>' + ctrl + '</td>';
        row += '<td style="display:none;"><input type="hidden" class="placement" /></td>';
        row += '<td style="vertical-align: middle; text-align:center"><input type="checkbox" class="hasbackdrop"/></td>';
        row += '<td style="vertical-align: middle;"><input style="padding:5px;" type="text" class="text-center img-sq-40 delay"/></td>';
        row += '<td style="vertical-align: middle;"><input style="padding:5px" type="text" class="text-center img-sq-40 duration"/></td>';
        row += '<td style="vertical-align: middle; text-align:center"><input type="checkbox" class="trigger"/></td>';
    if(style){
            row += '<td style="display:none;"><input class="wlkTxtArCopy" data-selector="' + elem.id + '" id="' + elem.id + '"></td>';
  }else {
    row += '<td style="display:none;"><input class="wlkTxtArCopy" data-selector="' + elem.element + '" id="' + elem.element + '"></td>';
  }
        row += '</tr>';

        return row;
}

function AddManualElementsToForm(elements) {
    for (var i = 0; i < elements.length; i++) {
        lastrowNum = i + 1;
        if (jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr').length == 1) {
          jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr:nth-child(' + lastrowNum + ')').after('<tr class="warning elements"><td class="srno">' + lastrowNum + '</td><td style="text-align: center; vertical-align: middle;width:20%;display:none;">' + elements[i].type + '</td>\
          <td style="  vertical-align: middle; width:10%"><input type="text" readonly style="border: 2px solid #FFC000;text-align: center;padding: 10px 0px 10px 0px;font-family:Open Sans Condensed;width:60px"></td><td class="valignMid text-center" width="10%"><input type="text" class="heading" style="border: 2px solid #FFC000;font-family:Open Sans Condensed;"></td>\
          <td style="vertical-align: middle;text-align:left" width="40%"><textarea class="wlkTxtAr" data-selector="' + elements[i].element + '" id="' + elements[i].element + '" rows="3"></textarea>\
          <button type="button" class="btn-wt btn-info-wt btn-xs-wt rteLauncher " data-toggle="modal-wt" style="display: -webkit-box" ><i class="fa fa-pencil"></i></button><button type="button" class="btn-wt btn-info-wt btn-xs-wt hideeditor" >Close Editor</button></td>\
          <td style="vertical-align: middle;text-align:center" width="20%"><input type="checkbox" class="selectme check" /></td><td style="display:none;"><input type="hidden" class="elementId" /></td>\
          <td>' + controllerControl + '</td><td style="display:none;"><input type="hidden" class="placement" /></td><td style="vertical-align: middle; text-align:center"><input type="checkbox" class="hasbackdrop"/></td>\
          <td><input type="text" class="delay"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="text" class="duration"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="checkbox" class="trigger"  style="padding-left: 7%;vertical-align: middle;" align="center"/></td>\
          <td style="display:none;"><input class="wlkTxtArCopy" data-selector="' + elements[i].element + '" id="' + elements[i].element + '"></td>\
          </tr>');
            var newRow = constructRow(lastrowNum,elements[i],controllerControl);

//            jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr:nth-child(' + lastrowNum + ')').after(newRow);
        } else {
            var checkSelector = '#walkthroughSideTable tr td textarea.wlkTxtAr[data-selector="' + elements[i].element + '"]';
            if (jQuery('#sidebarframe').contents().find(checkSelector).length === 0) {
              jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr:nth-child(' + lastrowNum + ')').after('<tr class="warning elements"><td class="srno">' + lastrowNum + '</td><td style="text-align: center; vertical-align: middle;width:20%;display:none;">' + elements[i].type + '</td>\
  <td style="  vertical-align: middle; width:10%"><input type="text" readonly style="border: 2px solid #FFC000;text-align: center;padding: 10px 0px 10px 0px;font-family:Open Sans Condensed;width:60px"></td><td class="valignMid text-center" width="10%"><input type="text" class="heading" style="border: 2px solid #FFC000;font-family:Open Sans Condensed;"></td>\
  <td style="vertical-align: middle;text-align:left" width="40%"><textarea class="wlkTxtAr" data-selector="' + elements[i].element + '" id="' + elements[i].element + '" rows="3"></textarea>\
  <button type="button" class="btn-wt btn-info-wt btn-xs-wt rteLauncher" data-toggle="modal-wt" style="display: -webkit-box"><i class="fa fa-pencil"></i></button><button type="button" class="btn-wt btn-info-wt btn-xs-wt hideeditor" >Close Editor</button></td>\
  <td style="vertical-align: middle;text-align:center" width="20%"><input type="checkbox" class="selectme check" /></td><td style="display:none;"><input type="hidden" class="elementId" /></td>\
  <td>' + controllerControl + '</td><td style="display:none;"><input type="hidden" class="placement" /></td><td style="vertical-align: middle; text-align:center"><input type="checkbox" class="hasbackdrop"/></td>\
  <td><input type="text" class="delay"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="text" class="duration"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="checkbox" class="trigger"  style="padding-left: 7%;vertical-align: middle;" align="center"/></td>\
  <td style="display:none;"><input class="wlkTxtArCopy" data-selector="' + elements[i].element + '" id="' + elements[i].element + '"></td>\
  </tr>');
                var newRow = constructRow(lastrowNum,elements[i],controllerControl);
          //      jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr:nth-child(' + lastrowNum + ')').after(newRow);
            }
        }

        registerRTELauncher();
    }

}

function fillFormValues(elemValues, backdrop) {
    if (backdrop) {
        $('#sidebarframe').contents().find('#walkthroughSideTable').next().find('tr td input').prop('checked', true);
    }

    for (var i = 0; i <= elemValues.length - 1; i++) {
        var tableTarget = '#walkthroughSideTable tr td textarea.wlkTxtAr[data-selector="' + elemValues[i].element + '"]';
        var tableTargetCopy = '#walkthroughSideTable tr td input.wlkTxtArCopy[data-selector="' + elemValues[i].element + '"]'
        var targetContent = elemValues[i].content;
        var targetTitle = elemValues[i].title;
         var heading = elemValues[i].heading;
        var elementId = elemValues[i].elementId;
        var placement = elemValues[i].placement != null || elemValues[i].placement != undefined ? elemValues[i].placement : 'right';
        var backdrop = elemValues[i].backdrop != null ? elemValues[i].backdrop : false;
        var trigger = elemValues[i].trigger != null ? elemValues[i].trigger : false;
        var duration = elemValues[i].duration != null ? elemValues[i].duration : 0;
        var delay = elemValues[i].delay != null ? elemValues[i].delay : 0;
        if (targetContent === "Walkthrough") {
            targetContent = '';
        }
        jQuery('#sidebarframe').contents().find(tableTarget).val(targetContent);
        jQuery('#sidebarframe').contents().find(tableTargetCopy).val(getPlainText(targetContent));
                jQuery('#sidebarframe').contents().find(tableTarget).parent().prev().find('input').val(heading);

        jQuery('#sidebarframe').contents().find(tableTarget).parent().prev().prev().find('input').val(targetTitle);
        jQuery('#sidebarframe').contents().find(tableTarget).parent().next().find('input').prop('checked', 'true');
        jQuery('#sidebarframe').contents().find(tableTarget).parent().nextAll().find('.elementId').val(elementId);
        jQuery('#sidebarframe').contents().find(tableTarget).parent().parent().find('td input.placement').val(placement);
        jQuery('#sidebarframe').contents().find(tableTarget).parent().parent().find('td input.hasbackdrop').prop('checked', backdrop);
        jQuery('#sidebarframe').contents().find(tableTarget).parent().parent().find('td input.trigger').prop('checked', trigger);
        jQuery('#sidebarframe').contents().find(tableTarget).parent().parent().find('td input.duration').val(duration);
        jQuery('#sidebarframe').contents().find(tableTarget).parent().parent().find('td input.delay').val(delay);
        jQuery('#sidebarframe').contents().find(tableTarget).parent().parent().find('div.controller span.fa').each(function() {
            if (jQuery(this).data('dir') == placement) {
                jQuery(this).addClass('active-pos');
            }
        });
        var third = targetTitle.split('Step ')[1];
        jQuery('#sidebarframe').contents().find(tableTarget).closest("tr").insertAfter(jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (parseInt(third)) + ")"))
    }
}

function startTour(stepsArr, name, backdrop, forSite) {
   chrome.storage.local.get("BackUrl",function(item){

   });
    //with walkthrough check if the url are same if yes then give restart option else not

    if ((stepsArr.length <= parseInt(window.localStorage.getItem('resume_step')) || !window.localStorage.getItem('resume_step') || window.localStorage.getItem('resume_step')<0) ) {
        // var r = confirm("Restart Walkthrough");
        // if (r == true) {
            window.localStorage.clear();
        // } else {
        //     //noaction
        // }
    }
    imgSrc = chrome.extension.getURL("libs/greyed.png");
    var tour = new Tour({
        container: "body",
        name: name.toString(),
        keyboard: true,
        autoscroll: true,
        debug: true,
        backdrop: backdrop,
        backdropContainer: 'body',
        steps: stepsArr,
        template: "<div style='z-index:2147483647' class='popover-wt mdl-color--white mdl-shadow--2dp tour fade bottom in wt-css'><div class='arrow'></div><div class='mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet noPadding'><p class='popover-title-wt'></p></div><div class='popover-content-wt'> </div><div class='popover-navigation-wt'><ul class='navig-btns'><li><p style='font-size: 7px; color:#abdaf3; margin:10px 0px 0px 0px; font-family:Roboto,sans-serif;'>POWERED BY<img src='" + imgSrc + "' width='35px' style='margin-top:-3px;'></p></li><li><button class='mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent' data-role='next'>GOT IT</button></li><li><button class='mdl-button mdl-js-button mdl-js-ripple-effect' data-role='prev'>BACK</button></li></ul></div> <div class='bottomborder'></div></div>",
        afterGetState: function(key, value) {},
        afterSetState: function(key, value) {},
        afterRemoveState: function(key, value) {},
        onStart: function(tour) {
           
        },
        onEnd: function(tour) {
           
            sendEndTourData(tour);
            window.localStorage.setItem('resume_step', '0');
            tour.setCurrentStep(0);
            //send tour end data here 

           
        },
        onShow: function(tour) {

        },
        onShown: function(tour) {
        jQuery('html').on('mouseup', function(e) {
        if(!$(e.target).closest('.popover-wt').length) {
        jQuery('.popover-wt').each(function(){
            
            tour.end();
        });
    }
});

            analyticStart(tour);


            EnableBackButtonForMultiPage(tour);
            var x = jQuery(tour._options.steps[tour._current].element).is(':visible');
            if (!x) {
                window.localStorage.setItem('resume_step', (tour._current + 1));
                tour.goTo(tour._current + 1);
            }

            $(function() {
                //this function is for the popup change
                $('#step_number').on('change', function() {
                    var values = $(this).val();

                });
                // this function is the function
               gotoStepCall(tour);
            });

        },
        onHide: function(tour) {},
        onHidden: function(tour) {},
        onNext: function(tour) {
         if(tour._options.steps[tour._current].trigger){   
         jQuery(tour._options.steps[tour._current].element).click();
         }
         if((tour._options.steps.length-1) ==tour._current){
                 sendEndTourData(tour);
            window.localStorage.setItem('resume_step', '0');
            tour.setCurrentStep(0);
            //send tour end data here 


         }else{
        
            var analyticdata = localStorage.getItem(tour._options.name);
            if (analyticdata != "" && analyticdata != null) {
            var analytic = JSON.parse(analyticdata);
                        console.log("Next");
            var obj = analytic.steps[tour._options.steps[tour._current].ElementId];
            if (obj) {
                obj.end = new Date();
                obj.StepId = tour._options.steps[tour._current].StepId;
                localStorage.setItem(tour._options.name, JSON.stringify(analytic));
            } else {
                analytic.steps[tour._options.steps[tour._current].ElementId] = {
                start: new Date(),
                end: null,
                StepId: tour._options.steps[tour._current].StepId
                };

                localStorage.setItem(tour._options.name, JSON.stringify(analytic));
            }

            /*******************Setting previous element time****************** */
            var analytic = JSON.parse(analyticdata);

            var newobj = analytic.steps[tour._options.steps[tour._current].ElementId];
            console.log(newobj);
            if (newobj) {
                newobj["end"] = new Date();
                newobj.StepId = tour._options.steps[tour._current].StepId;
                localStorage.setItem(tour._options.name, JSON.stringify(analytic));
            }
            /*******************Setting previous element time****************** */

            var analyticdata = localStorage.getItem(tour._options.name);
            console.log(analyticdata);
            }

            window.localStorage.setItem("resume_step", tour.getCurrentStep() + 1);

         }



        },
        onPrev: function(tour) {
             window.localStorage.setItem('resume_step', (tour.getCurrentStep() - 1));
        },
        onPause: function(tour, duration) {},
        onResume: function(tour, duration) {},
        onRedirectError: function(tour) {}
    });
    tour._options.steps = [];
    tour.addSteps(stepsArr);
    tour.init(true);
    tour.start(true);



    var stepno = parseInt(window.localStorage.getItem('resume_step')) ? parseInt(window.localStorage.getItem('resume_step')) : 0;
    tour.setCurrentStep(stepno);

    if (tour.ended()) {
        // decide what to do
        tour.restart();
    }
}
function analyticStart(tourdata){
    //tour starts 
    console.log('tour start');
    console.log(tourdata);

     var analyticdata = localStorage.getItem(tourdata._options.name);
 
     if(analyticdata != '' && analyticdata != null){
     
        var analytic=JSON.parse(analyticdata);
        //analytic.push({"WalkthroughId":tourdata._options.name, "StartTime":new Date()});
      //  localStorage.setItem(tourdata._options.name, JSON.stringify(analytic));
        
     }else{
        //if(tourdata._current==0){
            var fiststep=tourdata._options.steps[tourdata._current].ElementId;
            var steps={};
            steps[fiststep]={start:new Date(),end:null,StepId:tourdata._options.steps[tourdata._current].StepId};
            
        localStorage.setItem(tourdata._options.name, JSON.stringify({"WalkthroughId":tourdata._options.name, "StartTime":new Date(),"UserName":"Anonymous",'steps':steps}));
       // }
     
     }
        var analyticdata = localStorage.getItem(tourdata._options.name);

        console.log(analyticdata);
}
function sendEndTourData(tourdata){
  //tour end 
            console.log('tour end');
            console.log(tourdata);

      var analyticdata = localStorage.getItem(tourdata._options.name);
       
     if(analyticdata != '' && analyticdata != null){
           var analytic=JSON.parse(analyticdata);
            analytic.EndTime=new Date();
           // alert((parseInt(tourdata._current)+1))
            analytic.LastElementSeenId=tourdata._options.steps[tourdata._current].ElementId;

            if((parseInt(tourdata._current)+1) == tourdata._options.steps.length){
             analytic.IsWalkthroughComplete=true;
            }else{

                analytic.IsWalkthroughComplete=false;
            }
             
            localStorage.setItem(tourdata._options.name, JSON.stringify(analytic));
           

                //API CALL HERE 

                  var obj=analytic.steps[tourdata._options.steps[tourdata._current].ElementId];

            if(obj){
               obj.end=new Date();
               obj.StepId=tourdata._options.steps[tourdata._current].StepId;
             localStorage.setItem(tourdata._options.name, JSON.stringify(analytic));

            }
                 var analyticdata = localStorage.getItem(tourdata._options.name);
               console.log('last element check');
                console.log(analyticdata);
               walkthroughendApi(JSON.parse(analyticdata));

         // walkthroughendApi(JSON.stringify(analyticdata));
         }
            

}
function walkthroughendApi(data){
    jQuery.ajax({
                    url: baseURL + "/api/walkthroughAnalytics/Add",
                    type: "POST",
                    data:{
                    
                    WalkthroughId:data.WalkthroughId,
                    UserName:data.UserName,
                    StartTime:data.StartTime,
                    EndTime:data.EndTime,
                    LastElementSeenId:data.LastElementSeenId,
                    IsWalkthroughComplete:data.IsWalkthroughComplete,
                    
                    steps:JSON.stringify(data.steps)
                        
                        
                        },
                    headers: {
                        xaccesstoken: token
                    },
                    // contentType: "application/json; charset=utf-8",
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    dataType: "json",
                    success: function(response) {
                        localStorage.removeItem(data.WalkthroughId);
                        if(!response.success){
                        
                        }else{   
                       
                        }
                    },
                    error: function(err) {}
                });
}
function showWalkthrough() {
    jQuery('#sidebarframe').contents().find('#addWalkthrough').hide();
    var sidebarContent = document.createElement('div');
    sidebarContent.id = "mySidebar";
    sidebarContent.innerHTML = walthroughElements(getInputElements());
    jQuery('#sidebarframe').contents().find('body').append(sidebarContent);
    sidebarOpen = true;
    myDomOutline.start();
    registerCheckboxClick();
    registerSaveElemClick();
    registerRunClick();
    registerUpdateClick();
    registerPanelClick();
    registerBackFromEditScreenClick();
    registerDeletePageClick();
    registerRTELauncher();
    registerFlipFormPosition();
    registerEnableSort();
}

function walthroughElements(elements) {
    var count = elements.length;
    var innerHtml = '<div class="table-responsive"><table class="table" id="walkthroughSideTable"><tr style="text-align:center"><th style="border: 3px solid #4472C4;background-color: #DAE3F3;color: #4472C4;font-weight: 500;border-right: azure;" class="hidden">#</th>\
  <th style="border: 3px solid #ED7D31;background-color: #FBE5D6;color: #C49779;font-weight: 500;border-right: azure;display:none;">Type</th><th style="border: 3px solid #FFC000;background-color: #FFF2CC;color: #8D7017;font-weight: 500;border-right: azure;">Title</th><th style="border: 3px solid #FFC000;background-color: #FFF2CC;color: #8D7017;font-weight: 500;border-right: azure;">Heading</th>\
  <th style="border: 2px solid #70AD47;background-color: #E2F0D9;color: #658054;font-weight: 500;border-right: azure;">Description</th><th style="border: 3px solid #4472C4;background-color: #DAE3F3;color: #4472C4;font-weight: 500;">Visible</th>\
  <th style="display:none;"></th><th style="border: 3px solid #269abd;background-color: #DAE3F3;color: #269abc;font-weight: 500;">Placement</th><th style="display:none;"></th><th>Backdrop</th><th>Delay</th><th>Duration</th><th>Trigger</th></tr>';
    var pointer = elements;
    if (count > 0) {

        for (var i = 0; i < count; i++) {
            if (pointer[i].isManual) {
              var row_no = i + 1,
              elem = pointer[i],
              ctrl = controllerControl,
              style = "opacity: 0.6;background-color: #EEEEEE";

              //innerHtml += constructRow(row_no,elem,ctrl,style);
      //
                innerHtml += '<tr style="opacity: 0.6;background-color: #EEEEEE" class="elements"><td class="srno">' + (i + 1) + '</td><td style="text-align: center; vertical-align: middle;width:20%;display:none;">' + pointer[i].type + '</td>\
      <td class="valignMid text-center" width="10%"><input type="text" readonly class="img-sq-60 pad-top-10 pad-bot-10" style="border: 2px solid #FFC000;font-family:Open Sans Condensed;"></td> <td class="valignMid text-center" width="10%"><input type="text" class="heading" class="img-sq-60 pad-top-10 pad-bot-10" style="border: 2px solid #FFC000;font-family:Open Sans Condensed;"></td>\
      <td class="valignMid text-center" width="40%"><textarea class="wlkTxtAr" data-selector="' + pointer[i].id + '" id="' + pointer[i].id + '"></textarea><button type="button" class="btn-wt btn-info-wt btn-xs-wt rteLauncher" data-toggle="modal-wt" style="display: -webkit-box"><i class="fa fa-pencil"></i></button>\
      <button type="button" class="btn-wt btn-info-wt btn-xs-wt hideeditor" >Close Editor</button></td><td style="vertical-align: middle;text-align:center" width="20%"><input type="checkbox" class="selectme check" /></td><td style="display:none;"><input type="hidden" class="elementId" />\
      </td><td>' + controllerControl + '</td><td style="display:none;"><input type="hidden" class="placement" /></td><td><input type="checkbox" class="hasbackdrop"  style="padding-left: 7%;vertical-align: middle;" align="center"/></td>\
      <td><input type="text" class="delay"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="text" class="duration"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="checkbox" class="trigger"  style="padding-left: 7%;vertical-align: middle;" align="center"/></td>\
      <td style="display:none;"><input class="wlkTxtArCopy" data-selector="' + pointer[i].id + '" id="' + pointer[i].id + '"></td>\
      </tr>';
            } else if (pointer[i].visibility) {
              var row_no = i + 1,
              elem = pointer[i],
              ctrl = controllerControl;

              // innerHtml += constructRow(row_no,elem,ctrl);
                innerHtml += '<tr class="elements"><td class="srno">' + (i + 1) + '</td><td style="vertical-align: middle;align:center;width:20%;display:none;">' + pointer[i].type + '</td><td style="vertical-align: middle;" align="center" width="10%"><input type="text" readonly style="border: 2px solid #FFC000;text-align: center;padding: 10px 0px 10px 0px;font-family:Open Sans Condensed;width:60px"></td> <td class="valignMid text-center" width="10%"><input type="text" class="heading" class="img-sq-60 pad-top-10 pad-bot-10" style="border: 2px solid #FFC000;font-family:Open Sans Condensed;"></td>\
      <td style="vertical-align: middle;text-align:left" width="40%"><textarea class="wlkTxtAr" data-selector="' + pointer[i].id + '" id="' + pointer[i].id + '"></textarea><button type="button" class="btn-wt btn-info-wt btn-xs-wt rteLauncher" data-toggle="modal-wt" style="display: -webkit-box"><i class="fa fa-pencil"></i></button>\
      <button type="button" class="btn-wt btn-info-wt btn-xs-wt hideeditor" >Close Editor</button></td><td style="vertical-align: middle;text-align:center" width="20%"><input type="checkbox" class="selectme check" /></td>\
      <td style="display:none;"><input type="hidden" class="elementId" /></td><td>' + controllerControl + '</td><td style="display:none;"><input type="hidden" class="placement" /></td><td style="vertical-align: middle; text-align:center"><input type="checkbox" class="hasbackdrop"/></td>\
      <td><input type="text" class="delay"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="text" class="duration"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="checkbox" class="trigger"  style="padding-left: 7%;vertical-align: middle;" align="center"/></td>\
      <td style="display:none;"><input class="wlkTxtArCopy" data-selector="' + pointer[i].id + '" id="' + pointer[i].id + '"></td>\
      </tr>';
            } else {
              var row_no = i + 1,
              elem = pointer[i],
              ctrl = controllerControl,
              style = "opacity: 0.6";

            //  innerHtml += constructRow(row_no,elem,ctrl,style);
                innerHtml += '<tr style="opacity: 0.6" class="elements"><td class="srno">' + (i + 1) + '</td><td style="text-align: center; vertical-align: middle;align:center;width:20%;display:none;">' + pointer[i].type + '</td><td style="vertical-align: middle; align="center" width="10%">\
      <input type="text" readonly style="border: 2px solid #FFC000;text-align: center;padding: 10px 0px 10px 0px;font-family:Open Sans Condensed;width:60px"></td> <td class="valignMid text-center" width="10%"><input type="text" class="heading" class="img-sq-60 pad-top-10 pad-bot-10" style="border: 2px solid #FFC000;font-family:Open Sans Condensed;"></td><td style="vertical-align: middle;text-align:left" width="40%"><textarea class="wlkTxtAr" data-selector="' + pointer[i].id + '" id="' + pointer[i].id + '"></textarea>\
      <button type="button" class="btn-wt btn-info-wt btn-xs-wt rteLauncher" data-toggle="modal-wt" style="display: -webkit-box"><i class="fa fa-pencil"></i></button><button type="button" class="btn-wt btn-info-wt btn-xs-wt hideeditor" >Close Editor</button></td><td style="vertical-align: middle;text-align:center" width="20%"><input type="checkbox" class="selectme check" /></td>\
      <td style="display:none;"><input type="hidden" class="elementId" /></td><td>' + controllerControl + '</td><td style="display:none;"><input type="hidden" class="placement" /></td><td style="vertical-align: middle; text-align:center"><input type="checkbox" class="hasbackdrop"/></td>\
      <td><input type="text" class="delay"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="text" class="duration"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="checkbox" class="trigger"  style="padding-left: 7%;vertical-align: middle;" align="center"/></td>\
      <td style="display:none;"><input class="wlkTxtArCopy" data-selector="' + pointer[i].id + '" id="' + pointer[i].id + '"></td>\
      </tr>';
            }
        }
    }
    innerHtml += '</table></div>';
    innerHtml += '<div class="col-xs-12" id="editorButtonsTbl"><button class="btn-wt btn-primary-wt mg-rt-10" data-toggle="tooltip" data-placement="left"  id="savePageElems" title="Save Elements"><i class="fa fa-floppy-o"></i></button><button style="display: none;" class="btn-wt btn-primary-wt mg-rt-10" data-toggle="tooltip" data-placement="left"  id="updatePageElems" title="Update Elements"><i class="fa fa-refresh"></i></button><button class="btn-wt btn-primary-wt mg-rt-10" data-toggle="tooltip" data-placement="left"  id="runWalkthrough" title="Run Walkthrough"><i class="fa fa-play-circle"></i></button><button class="btn-wt btn-primary-wt mg-rt-10" data-toggle="tooltip" data-placement="left"  id="backFromEditScr" title="Back"><i class="fa fa-reply"></i></button><button class="btn-wt btn-primary-wt mg-rt-10" data-toggle="tooltip" data-placement="left"  id="flipSideForm" title="Switch Position"><i class="fa fa-exchange"></i></button><div class="checkbox pageBackDrop"> <label><input type="checkbox" value="">Back drop</label></div><div class="checkbox enableSort"> <label><input type="checkbox" value="">Enable Sort</label></div></div>';
    innerHtml += '<div class="col-xs-12" id="editorRefPanel"><div class="row"><div class="panel panel-primary" ><div class="panel-heading"><h3 class="panel-title">Walkthrough Pages</h3><span class="pull-right "><button class="btn-wt btn-primary-wt mg-rt-10 pull-right"  id="reorderPages" title="Save Page Order" style=""><i class="fa fa-floppy-o"></i></button></span></div><div id="panelContent" class="panel-body" style="overflow:auto;"></div></div></div></div>'
    return innerHtml;
}

function registerPanelClick() {

    jQuery('#sidebarframe').contents().find('body').on('click', '.panel-heading span.clickable', function(e) {
        var $this = jQuery(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    })
}

function registerEnableSort() {
    jQuery('#sidebarframe').contents().find('body').on('click', '#reorderPages', function() {

        savePageOrder();
    });

    jQuery('#sidebarframe').contents().find('body').on('click', '.enableSort input', function() {
        if (jQuery(this).is(':checked')) {
            makeTableSortable();
            makePageSortable();
        } else {
            jQuery('#sidebarframe').contents().find("#walkthroughSideTable tbody").sortable('disable');
        }
    });
}

function gotoStepCall(tour)
{
 

    var _tour = tour;
     $('#jumpbutton').on('click', function() {
      
                    var temp = document.getElementById('step_number').value;
                    temp = temp - 1;
                    _tour.goTo(temp);
                });
}

function savePageOrder() {
    var mydata = [];

    jQuery('#sidebarframe').contents().find('body').find('#pagesTable').children().children().each(function(i) {
        mydata[i] = {};
        mydata[i].StepId = jQuery(this).find('td:nth-child(2)').find('#deleteWalkthruPage').data('stepid');
        mydata[i].PageOrderNo = jQuery(this).find('td:nth-child(1)').html();
        mydata[i].Alias = jQuery(this).find('#alias').val();

    });

    jQuery.ajax({
        url: baseURL + '/api/walkthrough/reorderPages',
        type: "POST",
        data: JSON.stringify(mydata),
        headers: {
            xaccesstoken: token
        },
        contentType: "application/json; charset=utf-8",
        success: function(response) {
            console.log(response);
            $.alert({
                title: 'Success!',
                content: 'Reordered Successfully!',
                confirm: function() {},
                columnClass: 'confirm-width'
            });

        },
        error: function(jqXHR, exception) {
            if (jqXHR.status === 403) {
                handleError();
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }

    });
}

function makePageSortable() {
    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            jQuery(this).width($(this).width());
        });
        return ui;
    };
    var renumberPage = function(e, ui) {
        $('td.srno', ui.item.parent()).each(function(i) {

            var number = $(this).parent().find('td:nth-child(1)').is(':checked').html();

            $(this).html(i + 1);

        });
    }
    loadJQueryUI().then(function() {
        jQuery('#sidebarframe').contents().find("#pagesTable tbody").sortable({
            helper: fixHelper,
            iframeFix: true,
            appendTo: jQuery(window.frames["sidebarframe"].contentWindow.document.body),
            stop: renumberPage
        }).disableSelection();
    });
}


function makeTableSortable() {
    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            jQuery(this).width($(this).width());
        });
        return ui;
    };
    // var renumber = function(e, ui) {
    //     $('td.srno', ui.item.parent()).each(function(i) {
    //         if ($(this).parent().find('td .selectme').is(':checked')) 
    //         {

    //             console.log($(this).parent().find('td .selectme'));
    //             var number = $(this).parent().find('td:nth-child(3) input').val();
    //             var text = 'Step ' + (i + 1);
    //             $(this).parent().find('td:nth-child(3) input').val('');
    //             $(this).parent().find('td:nth-child(3) input').val(text);
    //             $(this).html(i + 1);
    //         }
    //     });
    // }
    var renumber = function(e,ui){
        var counter = 0;
        console.log(ui)
        jQuery(".elements.ui-sortable-handle",ui.item.parent()).find('td>.selectme').each(function(i){
           
            if(jQuery(this).is(':checked')){
                     var temp = jQuery(this);    
            var text = 'Step ' + (counter + 1);
            console.log(text);
            jQuery(this).parent().parent().find('td:nth-child(3) input').val('');
            jQuery(this).parent().parent().find('td:nth-child(3) input').val(text);
            jQuery(this).parent().parent().find('td.srno').html(counter + 1);
              counter++;   
                               

            }
              

           
            
        });

    }
    loadJQueryUI().then(function() {
        jQuery('#sidebarframe').contents().find("#walkthroughSideTable tbody").sortable({
            helper: fixHelper,
            iframeFix: true,
            appendTo: jQuery(window.frames["sidebarframe"].contentWindow.document.body),
            stop: renumber
          
        }).disableSelection();
    });
}

// function setUpEditor()
// {
//   AppendModal();
// }

function getInputElements() {
    var formElements = [];
    jQuery("textarea,checkbox,radio,button,select,input,a[id*='sign'],a[name*='sign'],a[id*='register'],a[name*='register'] iframe").each(function(index, elm) {
        var input_type = jQuery(this).find('input');
        var select_type = jQuery(this).find('select');
        var radio_type = jQuery(this).find(':radio');
        var checkbox_type = jQuery(this).find('checkbox');
        var button_type = jQuery(this).find('button');
        var textarea_type = jQuery(this).find('textarea');
        var submit_type = jQuery(this).find(':submit');
        var signUp = jQuery(this).find("a[id*='sign']") || jQuery.find("a[name*='sign']") || jQuery(this).find("a[id*='register']") || jQuery(this).find("a[name*='register']");
        var elmname = isNameEmpty(elm);
        var elmid = isIdEmpty(elm);
        var object = {};
        if (elm.type != 'hidden') {
            if (jQuery(elm).closest('.modal-wt-content').length == 0) {
                if (input_type) {
                    object = {
                        name: elmname,
                        type: elm.type,
                        id: jQuery(elm).getPath(),
                        visibility: jQuery(elm).is(':visible')
                    };
                    formElements.push(object);
                } else if (select_type) {
                    object = {
                        name: elmname,
                        type: 'select',
                        id: jQuery(elm).getPath(),
                        visibility: jQuery(elm).is(':visible')
                    };
                    formElements.push(object);
                } else if (radio_type) {
                    object = {
                        name: elmname,
                        type: 'radio',
                        id: jQuery(elm).getPath(),
                        visibility: jQuery(elm).is(':visible')
                    };
                    formElements.push(object);
                } else if (checkbox_type) {
                    object = {
                        name: elmname,
                        type: 'checkbox',
                        id: jQuery(elm).getPath(),
                        visibility: jQuery(elm).is(':visible')
                    };
                    formElements.push(object);
                } else if (button_type) {
                    object = {
                        name: elmname,
                        type: 'button',
                        id: jQuery(elm).getPath(),
                        visibility: jQuery(elm).is(':visible')
                    };
                    formElements.push(object);
                } else if (textarea_type) {
                    object = {
                        name: elmname,
                        type: 'textarea',
                        id: jQuery(elm).getPath(),
                        visibility: jQuery(elm).is(':visible')
                    };
                    formElements.push(object);
                } else if (submit_type) {
                    object = {
                        name: elmname,
                        type: 'Submit',
                        id: jQuery(elm).getPath(),
                        visibility: jQuery(elm).is(':visible')
                    };
                    formElements.push(object);
                } else if (signUp) {
                    object = {
                        name: elmname,
                        type: 'SignUp',
                        id: jQuery(elm).getPath(),
                        visibility: jQuery(elm).is(':visible')
                    };
                    formElements.push(object);
                }
            }
        }

    });

    return formElements;
}

function isNameEmpty(elm) {
    if (elm.name == '') {
        elm.name = elm.id;
    }
    return elm.name;
}

function isIdEmpty(elm) {
    if (elm.id == '') {
        elm.id = elm.name;
    }

    return elm.id;
}

function CheckVisible(elm) {
    return jQuery(elm).is(':visible');
}
//when element is check it rearranges it
function focusElem(row, index) {
    var elemId = row.find('td textarea.wlkTxtAr').attr('data-selector');
    jQuery(elemId).focus();
    var offset = jQuery(elemId).offset;
    jQuery(elemId).addClass('glowing-border');
    var n = parseInt(row.index());
    // set step value
    var fields = jQuery('#sidebarframe').contents().find(".check");
    var count = 0;
    for (i = 0; i < fields.length; i++) {
        if (fields[i].checked) {
            count = count + 1;
        }
    }
    jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (n + 1) + ") td:nth-child(3) input").val('Step ' + count);
    var prevCheck = jQuery(row).prev("tr").find('td:nth-child(5) input').is(':checked');

    if (prevCheck) {
        return;
    } else {
        var upperRows = jQuery(row).prevUntil("table", "tr");
        for (i = 2; i <= upperRows.length; i++) {
            if (!(jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (i) + ") td:nth-child(5) input").is(':checked'))) {
                row.insertAfter(jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (i - 1) + ")"))
                return;
            }
        }
    }
}

function looseFocusElem(row, index) {
    var elemId = row.find('td textarea.wlkTxtAr').attr('data-selector');
    jQuery(elemId).removeClass('glowing-border');
    var n = parseInt(row.index());
    var fieldType = row.find("td:nth-child(2)").text();
    var stepValue = row.find("td:nth-child(3) input").val();
    var textArea = row.find("td:nth-child(4) textarea.wlkTxtAr").val();
    var bool = row.find("td:nth-child(5) input").is(':checked');
    var rows = jQuery('#sidebarframe').contents().find('tr');
    var nextRows = row.nextUntil("table", "tr");
    for (i = n; i < rows.length; i++) {
        if (jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (i + 2) + ") td:nth-child(5) input").is(':checked')) {
            var third = jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (i + 2) + ") td:nth-child(3) input").val().split('Step ')[1];
            if (third != undefined) {
                jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (i + 2) + ") td:nth-child(3) input").val('Step ' + (parseInt(third) - 1));
            }
        } else {
            row.find("td:nth-child(3) input").val('');
            row.insertAfter(jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (i + 1) + ")"))
            return;
        }
    }
}

function reArrangeCheckedRow(row, index) {
    var n = parseInt(row.find('td').html());
    while (n !== index) {
        row.prev().before(row);
        n--;
        jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (n + 1) + ") td:first-child").html(n);
        jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr:nth-child(" + (n + 1) + ") td:nth-child(3) input").val('Step ' + n);
    }
    jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr td:first-child").each(function(index) {
        if (index == 0 && index == jQuery('#sidebarframe').contents().find("#walkthroughSideTable tr td:first-child").length - 1) {} else {
            jQuery(this).html(index + 1);
        }
    });
}
var index = 0;

function registerCheckboxClick() {
    jQuery('#sidebarframe').contents().find("body").on('click', '.check', function(e) {
        if (jQuery(this).is(':checked')) {
            index++;
            focusElem(jQuery(this).parent().parent(), index);
           // reArrangeCheckedRow(jQuery(this).parent().parent(), index);
        } else {
            looseFocusElem(jQuery(this).parent().parent(), index);
            index--;

             //reArrangeCheckedRow(jQuery(this).parent().parent(), index);
        }
         console.log(jQuery('#sidebarframe').contents().find("td>.selectme:checked").length);
        
    });
}
var clickedElement = null;
document.addEventListener('mousedown', function(event) {
    if (sidebarOpen) {
        if (event.button == 2) {
            clickedElement = event.target;
            var selectedElem = jQuery(event.target).getPath();
            var clickedElementId = jQuery(clickedElement).attr('data-selector');
            if (clickedElementId == undefined || clickedElementId == '') {
                clickedElementId = selectedElem;
            }
            $.confirm({
                title: 'Confirm!',
                content: "Would you like to add this element manually:<br/>(node type=" + jQuery(clickedElement).prop("nodeName") + ") to this walk thru?",
                confirmButton: 'Yes',
                cancelButton: 'No',
                columnClass: 'confirm-width',
                backgroundDismiss: false,
                confirm: function() {
                    var checkSelector = '#walkthroughSideTable tr td textarea.wlkTxtAr[data-selector="' + clickedElementId + '"]';
                    if (jQuery('#sidebarframe').contents().find(checkSelector).length !== 0) {
                        $.alert({
                            title: 'Conflict!',
                            content: 'Element already present in walkthrough!',
                            confirm: function() {},
                            columnClass: 'confirm-width'
                        });
                    } else {
                        var lastrowNum = jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr').length;
                        var manualObj = [];
                        var item = {};
                        item["id"] = clickedElementId;
                        item["name"] = jQuery(clickedElement).attr('name');
                        item["type"] = jQuery(clickedElement).prop("nodeName");
                        item["visibility"] = jQuery(selectedElem).is(':visible');
                        item["isManual"] = true;
                        manualObj.push(item);
                        jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr:nth-child(' + lastrowNum + ')').after('<tr class="elements warning"><td class="srno">' + lastrowNum + '</td>\
          <td style="text-align: center; vertical-align: middle;width:20%;display:none;">' + jQuery(clickedElement).prop("nodeName") + '</td><td style="  vertical-align: middle; align="center" width="10%">\
          <input type="text" readonly style="border: 2px solid #FFC000;text-align: center;padding: 10px 0px 10px 0px;font-family:Open Sans Condensed;width:60px"></td><td style="vertical-align: middle;text-align:center" width="10%"><input type="text" class="heading" style="border: 2px solid #FFC000;font-family:Open Sans Condensed;"></td><td style="vertical-align: middle;text-align:center" width="40%">\
          <textarea class="wlkTxtAr" data-selector="' + clickedElementId + '" id="' + clickedElementId + '"></textarea><button type="button" class="btn-wt btn-info-wt btn-xs-wt rteLauncher" data-toggle="modal-wt" style="display: -webkit-box" ><i class="fa fa-pencil"></i></button>\
          <button type="button" class="btn-wt btn-info-wt btn-xs-wt hideeditor" >Close Editor</button></td><td style="vertical-align: middle;text-align:center" width="20%"><input type="checkbox" class="selectme check" /></td>\
          <td style="display:none;"><input type="hidden" class="elementId" /></td><td>' + controllerControl + '</td><td style="display:none;"><input type="hidden" class="placement" /></td>\
          <td style="vertical-align: middle; text-align:center"><input type="checkbox" class="hasbackdrop"/></td>\
          <td><input type="text" class="delay"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="text" class="duration"  style="padding-left: 7%;vertical-align: middle;width:40px;" align="center"/></td><td><input type="checkbox" class="trigger"  style="padding-left: 7%;vertical-align: middle;" align="center"/></td></tr>');
                        setElementPlacement();
                        registerRTELauncher();
                    }

                    return;
                },
                cancel: function() {
                    return;
                }
            });
        }
    }
});
jQuery('body').on('mouseover', function(event) {
    if (sidebarOpen) {
        var selectedElem = jQuery(event.target).getPath();
        jQuery(selectedElem).on('mouseout', function(e) {
            var elemToFind = jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr td textarea.wlkTxtAr[data-selector="' + jQuery(this).getPath() + '"]');
            elemToFind.parent().parent().removeClass('glowing-border');
        });
        var elemToFind = jQuery('#sidebarframe').contents().find('#walkthroughSideTable tr td textarea.wlkTxtAr[data-selector="' + selectedElem + '"]');
        if (elemToFind.length != 0) {
            elemToFind.parent().parent().addClass('glowing-border');
            elemToFind.focus();
        }
    }
});

function setPopUpTemplate(tour_steps) {
    $.each(tour_steps, function(i, step) {
        step['title'] = step['title'].replace('Step ','');  

        step['title']=step['title'].replace('/','');
        step['title'] += '<span class="stepcount" style="font-weight:500 !important;"> of ' + tour_steps.length + '</span>';
        var percent = parseInt(((i + 1) / tour_steps.length) * 100);
        step['content'] = '<div class="popover-title-text-wt" style="color:white; font-size:20px; font-family:Titillium,sans-serif; margin: -13px 0 5px 0;">'+step['heading']+'</div><div class="popover-text-wt">' + step['content'] + '</div>' + "" + '</div>';
    });
}



function setElementPlacement() {
    jQuery('#sidebarframe').contents().find("div.controller span.fa").on('click', function() {
        $(this).parent().find('span.fa').removeClass('active-pos');
        $(this).addClass('active-pos');
        $(this).parent().parent().next().find('input').val($(this).data('dir'));
    });
}

function RegisterWindowUnload() {
    jQuery(window).unload(function() {
        chrome.storage.local.set({
            "PreviousPage": ''
        }, function() {
            chrome.storage.local.get("CurrentWalkthrough", function(item) {

                chrome.storage.local.get("CurrentWalkthroughDetails", function(details) {

                    if (item.CurrentWalkthrough.Id == details.walkthroughId && details.forPage != null && details.forPage != undefined) {
                        chrome.storage.local.set({
                            "PreviousPage": details.forPage
                        }, function() {
                            if (chrome.runtime.error) {
                                console.log("Runtime error.");
                            }
                        });
                    }
                });
            });
        });
    });
}

function registerFlipFormPosition() {
    jQuery('#sidebarframe').contents().find('#flipSideForm').on('click', function() {
        if (leftPlacement) {
            jQuery('body').css('padding-left', '0%');
            jQuery('#sidebarframe').css('float', '').css('float', 'right').css('left', '').css('right', '0px');
            jQuery('body').css('padding-right', '40%');
            leftPlacement = false;
        } else {
            jQuery('body').css('padding-left', '40%');
            jQuery('#sidebarframe').css('float', '').css('float', 'left').css('left', '0px').css('right', '');
            jQuery('body').css('padding-right', '0%');
            leftPlacement = true;
        }
    });
}

function copyHelper() {
    $("#clickme").click(function() {
        $('#copyme').select();
        if (document.execCommand('copy'))
            $.alert({
                title: 'Success!',
                content: 'Link copied!',
                confirm: function() {},
                columnClass: 'confirm-width'
            });
    });
}

function registerRTELauncher() {

    jQuery('#sidebarframe').contents().find('.hideeditor').hide();
    jQuery('#sidebarframe').contents().find('.rteLauncher').on('click', function() {
        AppendModal();
        jQuery('#customEditor').modal('show');
        var contentToEdit = jQuery(this).prev().val();
        tinymce.activeEditor.setContent(contentToEdit);
        RegisterContentUpdate(jQuery(this).prev().data('selector'));
        jQuery(this).hide();
        jQuery(this).next().show();
        jQuery(this).next().css("display","-webkit-box");

        jQuery('#sidebarframe').contents().find('.hideeditor').on('click', function() {
            jQuery(this).hide();
            jQuery(this).prev().show();
            tinymce.remove();
            jQuery('#customEditor').remove();
            jQuery('#customEditor').modal('hide');
            jQuery('body').removeClass('wt-css');
        });


    });
}

function EnableBackButtonForMultiPage(tour) {
    var thisTour = tour;
    jQuery.ajax({
        url: baseURL + "/api/walkthrough/getAllSteps?walkthroughId=" + tour._options.name,
        type: "GET",
        dataType: "json",
        headers: {
            xaccesstoken: token
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function(data) {
            if (data.length > 1 && thisTour.getCurrentStep() == 0 && window.location.pathname !== data[0].Page) {
                jQuery('.btn-wt.back').attr('disabled', false).removeClass('disabled');
                var index = GetPreviousPageIndex(data);
                OnBackClick(data[index].Page, data[index].AdditionalParam);
            }
            onMinimize(thisTour);
        },
        error: {},
    });
}


function OnBackClick(page, params) {
    var backPage = (params) ? (page + params) : page;
    jQuery('.btn-wt.back').on('click', function() {
        console.log(backPage);
        window.location.href = backPage;
    });
}

function onMinimize(tour) {

    registerMinimizeClick(tour);

    if (stepMinimized) {
        HidePopOverContentAndReduceSize(tour);
    }

}

function registerMinimizeClick(tour) {
    var thisTour = tour;
    jQuery("div[data-dismiss='modal-wt']").on('click', function() {
        if (!stepMinimized) {
            HidePopOverContentAndReduceSize(thisTour);

        } else {
            UnHidePopOverContentAndRestoreSize(thisTour);
            stepMinimized = false;
        }
    });
}

function HidePopOverContentAndReduceSize(tour) {

    imgSrc = chrome.extension.getURL("libs/WalkThru_Logo_v6_38x38px.png");
    nImgSrc = chrome.extension.getURL("libs/next.png");
    jQuery('p.popover-title-wt').hide();
    jQuery('div.popover-content-wt').hide();
    jQuery('div.bottomborder').hide();
    jQuery('.btn-wt.back').hide();
    jQuery('button[data-role="end"]').hide();
    jQuery('button[data-role="next"]').html('<span><img src="' + imgSrc + '" height="18px" width="18px"/></span>');
    jQuery("div[data-dismiss='modal-wt']").html('&plus;');
    jQuery('.tour.popover-wt').css('min-width', '75px').css('max-width', '80px');
    stepMinimized = true;
    repositionTip(tour);
}

function UnHidePopOverContentAndRestoreSize(tour) {
    jQuery('.btn-wt.back').show();
    jQuery('p.popover-title-wt').show();
    jQuery('div.popover-content-wt').show();
    jQuery('div.bottomborder').show();
    jQuery('button[data-role="end"]').show();
    jQuery("div[data-dismiss='modal-wt']").html('&minus;');
    jQuery('button[data-role="next"]').html('Next');
    jQuery('.tour.popover-wt').css('min-width', '310px');
    stepMinimized = false;
    repositionTip(tour);
     gotoStepCall(tour);
}

function GetPreviousPageIndex(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].Page === window.location.pathname) {
            return (i == 0) ? i : i - 1
        }
    }
    return 0;
}

function repositionTip(tour) {
    tour._showPopover(tour._options.steps[tour.getCurrentStep()], tour.getCurrentStep());
    registerMinimizeClick(tour);
}

function AppendModal() {

    var modal = '<div class="modal-wt" data-backdrop="static" style="" id="customEditor"  data-keyboard="false" role="dialog"><div class="confirm-width"><div class="modal-wt-content"><div class="modal-wt-header">\
        <h4 class="modal-wt-title">Text Editor</h4></div><div class="modal-wt-body" ><p><textarea id="wlkSummernoteEdtior"></textarea><div id="updateSideBarContent" class="btn-wt btn-primary-wt btn-block-wt" style="padding: 7px;margin: 10px 0;color: #FFF;cursor: pointer;">Update Content</div></p></div></div></div>';
    jQuery('body').addClass('wt-css').append(modal);
    tinymce.init({
        selector: "#wlkSummernoteEdtior",
        plugins: ['link image paste textcolor media'],
        menubar: false,
        toolbar1: "undo redo | styleselect |  removeformat bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        toolbar2: "removeformat| fontselect fontsizeselect | paste | forecolor backcolor",
        paste_data_images: true,
        textcolor_cols: "5"
    });

}

function RegisterContentUpdate(selector) {
    var currentSelector = selector;
    jQuery('#updateSideBarContent').on('click', function() {
        var htmlUpdatedContent = tinymce.activeEditor.getContent({
            format: 'raw'
        });
        var textUpdatedContent = tinymce.activeEditor.getContent({
            format: 'text'
        });
        jQuery('#sidebarframe').contents().find('.hideeditor').click();

        jQuery('#sidebarframe').contents().find('textarea.wlkTxtAr[data-selector="' + currentSelector + '"').val(htmlUpdatedContent);
        jQuery('#sidebarframe').contents().find('input.wlkTxtArCopy[data-selector="' + currentSelector + '"').val(getPlainText(htmlUpdatedContent));
        // jQuery('body').removeClass('wt-css');
        // jQuery('#customEditor').modal('hide');
        tinymce.remove();
        $(".modal-wt,.modal-wt-backdrop").remove();
        jQuery('#customEditor').remove();
        jQuery('body').removeClass('wt-css');
        jQuery('body').find('.hideeditor').css('display','none');
        jQuery('body').find('.rteLauncher').css('display','-webkit-box');

    });
}


jQuery.fn.bootcomplete = function(options) {

    var defaults = {
        url: baseURL + '/api/walkthrough/FindBySite?site=',
        method: 'get',
        wrapperClass: "bc-wrapper",
        menuClass: "bc-menu",
        idField: true,
        idFieldName: $(this).attr('name') + "_id",
        minLength: 3,
        dataParams: {},
        formParams: {},

    }

    var settings = $.extend({}, defaults, options);

    $(this).attr('autocomplete', 'off')
    $(this).wrap('<div class="' + settings.wrapperClass + '"></div>')
    if (settings.idField) {
        $('<input type="hidden" id="' + settings.idFieldName + '"  name="' + settings.idFieldName + '" value="">').insertBefore($(this))
    }
    $('<div class="' + settings.menuClass + ' list-group"></div>').insertAfter($(this))

    $(this).on("keyup", searchQuery);

    var xhr;
    var that = $(this)

    function searchQuery() {

        var arr = [];
        $.each(settings.formParams, function(k, v) {
            arr[k] = $(v).val()
        })
        var dyFormParams = $.extend({}, arr);
        var Data = $.extend({
            site: $(this).val()
        }, settings.dataParams, dyFormParams);

        if (!Data.query) {
            $(this).next('.' + settings.menuClass).html('')
            $(this).next('.' + settings.menuClass).hide()
        }

        if (Data.site.length >= settings.minLength) {

            if (xhr && xhr.readyState != 4) {
                xhr.abort();
            }

            xhr = $.ajax({
                type: settings.method,
                url: settings.url,
                data: Data,
                dataType: "json",
                success: function(json) {
                    if (json && json.length >= 1) {
                        var results = ''
                        $.each(json, function(i, j) {
                            results += '<a href="#" class="list-group-item" data-id="' + j.Id + '" data-label="' + j.Name + '">' + j.Name + '</a>'
                        });

                        $(that).next('.' + settings.menuClass).html(results)
                        $(that).next('.' + settings.menuClass).children().on("click", selectResult)
                        $(that).next('.' + settings.menuClass).show()
                    }
                },
                error: function(err) {

                }
            })
        }
    }

    function selectResult() {
        $(that).val($(this).data('label'))
        if (settings.idField) {
            $(that).prev('input[name="' + settings.idFieldName + '"]').val($(this).data('id'))
        }
        $(that).next('.' + settings.menuClass).hide()
        return false;
    }

    return this;
};
