console.log( 'Background.html starting!' );
/*Put page action icon on all tabs*/
chrome.tabs.onUpdated.addListener(function(tabId) {
  chrome.pageAction.show(tabId);
});

chrome.tabs.getSelected(null, function(tab) {
  chrome.pageAction.show(tab.id);
});

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {          
 if (changeInfo.status == 'complete') {   
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
   chrome.tabs.sendMessage(tabs[0].id, {action: "checkAndRunWalkthrough"}, function(response) {});  
 });
}
});

/*Send request to current tab when page action is clicked*/
chrome.pageAction.onClicked.addListener(function(tab) {

  chrome.tabs.executeScript(null, { file: "libs/tinyMCE/tinymce.min.js" },function(){
    chrome.tabs.executeScript(null,{file:"libs/tinyMCE/plugins/link/plugin.min.js"},function(){
      chrome.tabs.executeScript(null,{file:"libs/tinyMCE/plugins/paste/plugin.min.js"},function(){
        chrome.tabs.executeScript(null,{file:"libs/tinyMCE/plugins/textcolor/plugin.min.js"},function(){  
           chrome.tabs.executeScript(null,{file:"libs/tinyMCE/plugins/media/plugin.min.js"},function(){ 
          chrome.tabs.executeScript(null,{file:"libs/tinyMCE/plugins/image/plugin.min.js"},function(){  
           chrome.tabs.executeScript(null,{file:"libs/tinyMCE/themes/modern/theme.min.js"},function(){
              chrome.tabs.insertCSS(null, {file: 'libs/tinyMCE/skins/lightgray/content.min.css'});  
           });
         });
        });
      });
    });
  });
  });

  chrome.tabs.getSelected(null, function(tab) {
   chrome.tabs.sendRequest(
				//Selected tab id
				tab.id,
				//Params inside a object data
				{callFunction: "toggleSidebar"}, 
				//Optional callback function
				function(response) {
					console.log(response);
				}
       );
 });
});


function onRequest(request, sender, callback) {
  if (request.action == 'getJSON') {

   $.getJSON(request.url, callback); 
 }
 if (request.action == 'ajax') 
 { 
   $.ajax({ 
    type : request.type,
    url : request.url,
    async : async,
    success : callback
  }); 
 } 
 if (request.action == 'get') { 
  $.get(request.url, callback); 
} 

if (request.action == 'post') { 

  $.post(request.url, request.data, callback); 

}
}

chrome.extension.onRequest.addListener(onRequest);


console.log( 'Background.html done.' );